static s16 D_global_asm_80611F0C[0x401] = {
    0x0000,
    0x000A,
    0x0014,
    0x001E,
    0x0028,
    0x0032,
    0x003D,
    0x0047,
    0x0051,
    0x005B,
    0x0065,
    0x0070,
    0x007A,
    0x0084,
    0x008E,
    0x0098,
    0x00A2,
    0x00AD,
    0x00B7,
    0x00C1,
    0x00CB,
    0x00D5,
    0x00E0,
    0x00EA,
    0x00F4,
    0x00FE,
    0x0108,
    0x0113,
    0x011D,
    0x0127,
    0x0131,
    0x013B,
    0x0146,
    0x0150,
    0x015A,
    0x0164,
    0x016E,
    0x0178,
    0x0183,
    0x018D,
    0x0197,
    0x01A1,
    0x01AB,
    0x01B6,
    0x01C0,
    0x01CA,
    0x01D4,
    0x01DE,
    0x01E9,
    0x01F3,
    0x01FD,
    0x0207,
    0x0211,
    0x021C,
    0x0226,
    0x0230,
    0x023A,
    0x0244,
    0x024F,
    0x0259,
    0x0263,
    0x026D,
    0x0277,
    0x0282,
    0x028C,
    0x0296,
    0x02A0,
    0x02AA,
    0x02B5,
    0x02BF,
    0x02C9,
    0x02D3,
    0x02DD,
    0x02E8,
    0x02F2,
    0x02FC,
    0x0306,
    0x0311,
    0x031B,
    0x0325,
    0x032F,
    0x0339,
    0x0344,
    0x034E,
    0x0358,
    0x0362,
    0x036D,
    0x0377,
    0x0381,
    0x038B,
    0x0395,
    0x03A0,
    0x03AA,
    0x03B4,
    0x03BE,
    0x03C9,
    0x03D3,
    0x03DD,
    0x03E7,
    0x03F1,
    0x03FC,
    0x0406,
    0x0410,
    0x041A,
    0x0425,
    0x042F,
    0x0439,
    0x0443,
    0x044E,
    0x0458,
    0x0462,
    0x046C,
    0x0477,
    0x0481,
    0x048B,
    0x0495,
    0x04A0,
    0x04AA,
    0x04B4,
    0x04BE,
    0x04C9,
    0x04D3,
    0x04DD,
    0x04E7,
    0x04F2,
    0x04FC,
    0x0506,
    0x0510,
    0x051B,
    0x0525,
    0x052F,
    0x053A,
    0x0544,
    0x054E,
    0x0558,
    0x0563,
    0x056D,
    0x0577,
    0x0581,
    0x058C,
    0x0596,
    0x05A0,
    0x05AB,
    0x05B5,
    0x05BF,
    0x05C9,
    0x05D4,
    0x05DE,
    0x05E8,
    0x05F3,
    0x05FD,
    0x0607,
    0x0612,
    0x061C,
    0x0626,
    0x0630,
    0x063B,
    0x0645,
    0x064F,
    0x065A,
    0x0664,
    0x066E,
    0x0679,
    0x0683,
    0x068D,
    0x0698,
    0x06A2,
    0x06AC,
    0x06B7,
    0x06C1,
    0x06CB,
    0x06D5,
    0x06E0,
    0x06EA,
    0x06F4,
    0x06FF,
    0x0709,
    0x0714,
    0x071E,
    0x0728,
    0x0733,
    0x073D,
    0x0747,
    0x0752,
    0x075C,
    0x0766,
    0x0771,
    0x077B,
    0x0785,
    0x0790,
    0x079A,
    0x07A4,
    0x07AF,
    0x07B9,
    0x07C4,
    0x07CE,
    0x07D8,
    0x07E3,
    0x07ED,
    0x07F7,
    0x0802,
    0x080C,
    0x0817,
    0x0821,
    0x082B,
    0x0836,
    0x0840,
    0x084B,
    0x0855,
    0x085F,
    0x086A,
    0x0874,
    0x087F,
    0x0889,
    0x0893,
    0x089E,
    0x08A8,
    0x08B3,
    0x08BD,
    0x08C8,
    0x08D2,
    0x08DC,
    0x08E7,
    0x08F1,
    0x08FC,
    0x0906,
    0x0911,
    0x091B,
    0x0926,
    0x0930,
    0x093A,
    0x0945,
    0x094F,
    0x095A,
    0x0964,
    0x096F,
    0x0979,
    0x0984,
    0x098E,
    0x0999,
    0x09A3,
    0x09AE,
    0x09B8,
    0x09C3,
    0x09CD,
    0x09D7,
    0x09E2,
    0x09EC,
    0x09F7,
    0x0A01,
    0x0A0C,
    0x0A16,
    0x0A21,
    0x0A2C,
    0x0A36,
    0x0A41,
    0x0A4B,
    0x0A56,
    0x0A60,
    0x0A6B,
    0x0A75,
    0x0A80,
    0x0A8A,
    0x0A95,
    0x0A9F,
    0x0AAA,
    0x0AB4,
    0x0ABF,
    0x0AC9,
    0x0AD4,
    0x0ADF,
    0x0AE9,
    0x0AF4,
    0x0AFE,
    0x0B09,
    0x0B13,
    0x0B1E,
    0x0B29,
    0x0B33,
    0x0B3E,
    0x0B48,
    0x0B53,
    0x0B5E,
    0x0B68,
    0x0B73,
    0x0B7D,
    0x0B88,
    0x0B93,
    0x0B9D,
    0x0BA8,
    0x0BB2,
    0x0BBD,
    0x0BC8,
    0x0BD2,
    0x0BDD,
    0x0BE8,
    0x0BF2,
    0x0BFD,
    0x0C07,
    0x0C12,
    0x0C1D,
    0x0C27,
    0x0C32,
    0x0C3D,
    0x0C47,
    0x0C52,
    0x0C5D,
    0x0C67,
    0x0C72,
    0x0C7D,
    0x0C87,
    0x0C92,
    0x0C9D,
    0x0CA8,
    0x0CB2,
    0x0CBD,
    0x0CC8,
    0x0CD2,
    0x0CDD,
    0x0CE8,
    0x0CF3,
    0x0CFD,
    0x0D08,
    0x0D13,
    0x0D1D,
    0x0D28,
    0x0D33,
    0x0D3E,
    0x0D48,
    0x0D53,
    0x0D5E,
    0x0D69,
    0x0D73,
    0x0D7E,
    0x0D89,
    0x0D94,
    0x0D9F,
    0x0DA9,
    0x0DB4,
    0x0DBF,
    0x0DCA,
    0x0DD5,
    0x0DDF,
    0x0DEA,
    0x0DF5,
    0x0E00,
    0x0E0B,
    0x0E15,
    0x0E20,
    0x0E2B,
    0x0E36,
    0x0E41,
    0x0E4C,
    0x0E56,
    0x0E61,
    0x0E6C,
    0x0E77,
    0x0E82,
    0x0E8D,
    0x0E98,
    0x0EA3,
    0x0EAD,
    0x0EB8,
    0x0EC3,
    0x0ECE,
    0x0ED9,
    0x0EE4,
    0x0EEF,
    0x0EFA,
    0x0F05,
    0x0F10,
    0x0F1A,
    0x0F25,
    0x0F30,
    0x0F3B,
    0x0F46,
    0x0F51,
    0x0F5C,
    0x0F67,
    0x0F72,
    0x0F7D,
    0x0F88,
    0x0F93,
    0x0F9E,
    0x0FA9,
    0x0FB4,
    0x0FBF,
    0x0FCA,
    0x0FD5,
    0x0FE0,
    0x0FEB,
    0x0FF6,
    0x1001,
    0x100C,
    0x1017,
    0x1022,
    0x102D,
    0x1038,
    0x1043,
    0x104E,
    0x1059,
    0x1064,
    0x106F,
    0x107B,
    0x1086,
    0x1091,
    0x109C,
    0x10A7,
    0x10B2,
    0x10BD,
    0x10C8,
    0x10D3,
    0x10DE,
    0x10EA,
    0x10F5,
    0x1100,
    0x110B,
    0x1116,
    0x1121,
    0x112C,
    0x1138,
    0x1143,
    0x114E,
    0x1159,
    0x1164,
    0x1170,
    0x117B,
    0x1186,
    0x1191,
    0x119C,
    0x11A8,
    0x11B3,
    0x11BE,
    0x11C9,
    0x11D5,
    0x11E0,
    0x11EB,
    0x11F6,
    0x1202,
    0x120D,
    0x1218,
    0x1223,
    0x122F,
    0x123A,
    0x1245,
    0x1251,
    0x125C,
    0x1267,
    0x1273,
    0x127E,
    0x1289,
    0x1295,
    0x12A0,
    0x12AB,
    0x12B7,
    0x12C2,
    0x12CD,
    0x12D9,
    0x12E4,
    0x12F0,
    0x12FB,
    0x1306,
    0x1312,
    0x131D,
    0x1329,
    0x1334,
    0x133F,
    0x134B,
    0x1356,
    0x1362,
    0x136D,
    0x1379,
    0x1384,
    0x1390,
    0x139B,
    0x13A7,
    0x13B2,
    0x13BE,
    0x13C9,
    0x13D5,
    0x13E0,
    0x13EC,
    0x13F7,
    0x1403,
    0x140E,
    0x141A,
    0x1426,
    0x1431,
    0x143D,
    0x1448,
    0x1454,
    0x145F,
    0x146B,
    0x1477,
    0x1482,
    0x148E,
    0x149A,
    0x14A5,
    0x14B1,
    0x14BD,
    0x14C8,
    0x14D4,
    0x14E0,
    0x14EB,
    0x14F7,
    0x1503,
    0x150E,
    0x151A,
    0x1526,
    0x1532,
    0x153D,
    0x1549,
    0x1555,
    0x1561,
    0x156C,
    0x1578,
    0x1584,
    0x1590,
    0x159C,
    0x15A7,
    0x15B3,
    0x15BF,
    0x15CB,
    0x15D7,
    0x15E3,
    0x15EE,
    0x15FA,
    0x1606,
    0x1612,
    0x161E,
    0x162A,
    0x1636,
    0x1642,
    0x164E,
    0x1659,
    0x1665,
    0x1671,
    0x167D,
    0x1689,
    0x1695,
    0x16A1,
    0x16AD,
    0x16B9,
    0x16C5,
    0x16D1,
    0x16DD,
    0x16E9,
    0x16F5,
    0x1701,
    0x170E,
    0x171A,
    0x1726,
    0x1732,
    0x173E,
    0x174A,
    0x1756,
    0x1762,
    0x176E,
    0x177A,
    0x1787,
    0x1793,
    0x179F,
    0x17AB,
    0x17B7,
    0x17C4,
    0x17D0,
    0x17DC,
    0x17E8,
    0x17F4,
    0x1801,
    0x180D,
    0x1819,
    0x1825,
    0x1832,
    0x183E,
    0x184A,
    0x1857,
    0x1863,
    0x186F,
    0x187C,
    0x1888,
    0x1894,
    0x18A1,
    0x18AD,
    0x18BA,
    0x18C6,
    0x18D2,
    0x18DF,
    0x18EB,
    0x18F8,
    0x1904,
    0x1911,
    0x191D,
    0x192A,
    0x1936,
    0x1943,
    0x194F,
    0x195C,
    0x1968,
    0x1975,
    0x1981,
    0x198E,
    0x199A,
    0x19A7,
    0x19B4,
    0x19C0,
    0x19CD,
    0x19DA,
    0x19E6,
    0x19F3,
    0x1A00,
    0x1A0C,
    0x1A19,
    0x1A26,
    0x1A32,
    0x1A3F,
    0x1A4C,
    0x1A59,
    0x1A65,
    0x1A72,
    0x1A7F,
    0x1A8C,
    0x1A99,
    0x1AA5,
    0x1AB2,
    0x1ABF,
    0x1ACC,
    0x1AD9,
    0x1AE6,
    0x1AF3,
    0x1B00,
    0x1B0C,
    0x1B19,
    0x1B26,
    0x1B33,
    0x1B40,
    0x1B4D,
    0x1B5A,
    0x1B67,
    0x1B74,
    0x1B81,
    0x1B8E,
    0x1B9C,
    0x1BA9,
    0x1BB6,
    0x1BC3,
    0x1BD0,
    0x1BDD,
    0x1BEA,
    0x1BF7,
    0x1C05,
    0x1C12,
    0x1C1F,
    0x1C2C,
    0x1C39,
    0x1C47,
    0x1C54,
    0x1C61,
    0x1C6E,
    0x1C7C,
    0x1C89,
    0x1C96,
    0x1CA4,
    0x1CB1,
    0x1CBE,
    0x1CCC,
    0x1CD9,
    0x1CE7,
    0x1CF4,
    0x1D02,
    0x1D0F,
    0x1D1C,
    0x1D2A,
    0x1D37,
    0x1D45,
    0x1D53,
    0x1D60,
    0x1D6E,
    0x1D7B,
    0x1D89,
    0x1D96,
    0x1DA4,
    0x1DB2,
    0x1DBF,
    0x1DCD,
    0x1DDB,
    0x1DE8,
    0x1DF6,
    0x1E04,
    0x1E12,
    0x1E1F,
    0x1E2D,
    0x1E3B,
    0x1E49,
    0x1E57,
    0x1E65,
    0x1E73,
    0x1E80,
    0x1E8E,
    0x1E9C,
    0x1EAA,
    0x1EB8,
    0x1EC6,
    0x1ED4,
    0x1EE2,
    0x1EF0,
    0x1EFE,
    0x1F0C,
    0x1F1A,
    0x1F29,
    0x1F37,
    0x1F45,
    0x1F53,
    0x1F61,
    0x1F6F,
    0x1F7E,
    0x1F8C,
    0x1F9A,
    0x1FA8,
    0x1FB7,
    0x1FC5,
    0x1FD3,
    0x1FE2,
    0x1FF0,
    0x1FFE,
    0x200D,
    0x201B,
    0x202A,
    0x2038,
    0x2047,
    0x2055,
    0x2064,
    0x2072,
    0x2081,
    0x208F,
    0x209E,
    0x20AD,
    0x20BB,
    0x20CA,
    0x20D9,
    0x20E7,
    0x20F6,
    0x2105,
    0x2114,
    0x2123,
    0x2131,
    0x2140,
    0x214F,
    0x215E,
    0x216D,
    0x217C,
    0x218B,
    0x219A,
    0x21A9,
    0x21B8,
    0x21C7,
    0x21D6,
    0x21E5,
    0x21F4,
    0x2204,
    0x2213,
    0x2222,
    0x2231,
    0x2240,
    0x2250,
    0x225F,
    0x226E,
    0x227E,
    0x228D,
    0x229D,
    0x22AC,
    0x22BB,
    0x22CB,
    0x22DA,
    0x22EA,
    0x22FA,
    0x2309,
    0x2319,
    0x2328,
    0x2338,
    0x2348,
    0x2358,
    0x2367,
    0x2377,
    0x2387,
    0x2397,
    0x23A7,
    0x23B7,
    0x23C6,
    0x23D6,
    0x23E6,
    0x23F6,
    0x2407,
    0x2417,
    0x2427,
    0x2437,
    0x2447,
    0x2457,
    0x2467,
    0x2478,
    0x2488,
    0x2498,
    0x24A9,
    0x24B9,
    0x24CA,
    0x24DA,
    0x24EB,
    0x24FB,
    0x250C,
    0x251C,
    0x252D,
    0x253E,
    0x254E,
    0x255F,
    0x2570,
    0x2581,
    0x2591,
    0x25A2,
    0x25B3,
    0x25C4,
    0x25D5,
    0x25E6,
    0x25F7,
    0x2608,
    0x2619,
    0x262B,
    0x263C,
    0x264D,
    0x265E,
    0x2670,
    0x2681,
    0x2693,
    0x26A4,
    0x26B5,
    0x26C7,
    0x26D9,
    0x26EA,
    0x26FC,
    0x270E,
    0x271F,
    0x2731,
    0x2743,
    0x2755,
    0x2767,
    0x2779,
    0x278B,
    0x279D,
    0x27AF,
    0x27C1,
    0x27D3,
    0x27E5,
    0x27F8,
    0x280A,
    0x281C,
    0x282F,
    0x2841,
    0x2854,
    0x2866,
    0x2879,
    0x288C,
    0x289F,
    0x28B1,
    0x28C4,
    0x28D7,
    0x28EA,
    0x28FD,
    0x2910,
    0x2923,
    0x2936,
    0x294A,
    0x295D,
    0x2970,
    0x2984,
    0x2997,
    0x29AB,
    0x29BE,
    0x29D2,
    0x29E6,
    0x29F9,
    0x2A0D,
    0x2A21,
    0x2A35,
    0x2A49,
    0x2A5D,
    0x2A71,
    0x2A85,
    0x2A9A,
    0x2AAE,
    0x2AC2,
    0x2AD7,
    0x2AEC,
    0x2B00,
    0x2B15,
    0x2B2A,
    0x2B3E,
    0x2B53,
    0x2B68,
    0x2B7D,
    0x2B93,
    0x2BA8,
    0x2BBD,
    0x2BD3,
    0x2BE8,
    0x2BFE,
    0x2C13,
    0x2C29,
    0x2C3F,
    0x2C55,
    0x2C6B,
    0x2C81,
    0x2C97,
    0x2CAD,
    0x2CC4,
    0x2CDA,
    0x2CF1,
    0x2D07,
    0x2D1E,
    0x2D35,
    0x2D4C,
    0x2D63,
    0x2D7A,
    0x2D91,
    0x2DA8,
    0x2DC0,
    0x2DD8,
    0x2DEF,
    0x2E07,
    0x2E1F,
    0x2E37,
    0x2E4F,
    0x2E67,
    0x2E80,
    0x2E98,
    0x2EB1,
    0x2ECA,
    0x2EE3,
    0x2EFC,
    0x2F15,
    0x2F2E,
    0x2F48,
    0x2F61,
    0x2F7B,
    0x2F95,
    0x2FAF,
    0x2FC9,
    0x2FE4,
    0x2FFE,
    0x3019,
    0x3034,
    0x304F,
    0x306A,
    0x3085,
    0x30A1,
    0x30BD,
    0x30D9,
    0x30F5,
    0x3111,
    0x312D,
    0x314A,
    0x3167,
    0x3184,
    0x31A2,
    0x31BF,
    0x31DD,
    0x31FB,
    0x321A,
    0x3238,
    0x3257,
    0x3276,
    0x3295,
    0x32B5,
    0x32D5,
    0x32F5,
    0x3316,
    0x3337,
    0x3358,
    0x3379,
    0x339B,
    0x33BD,
    0x33E0,
    0x3403,
    0x3426,
    0x344A,
    0x346E,
    0x3493,
    0x34B8,
    0x34DD,
    0x3503,
    0x352A,
    0x3551,
    0x3578,
    0x35A0,
    0x35C9,
    0x35F2,
    0x361D,
    0x3647,
    0x3673,
    0x369F,
    0x36CC,
    0x36FA,
    0x3729,
    0x3759,
    0x378A,
    0x37BB,
    0x37EF,
    0x3823,
    0x3859,
    0x3890,
    0x38C9,
    0x3904,
    0x3941,
    0x3980,
    0x39C1,
    0x3A05,
    0x3A4D,
    0x3A98,
    0x3AE7,
    0x3B3B,
    0x3B96,
    0x3BF8,
    0x3C65,
    0x3CE1,
    0x3D73,
    0x3E33,
    0x4000,
};
static f32 D_global_asm_80612710[32] = {
    0.17504200339317f,
    0.18187806010246f,
    0.18871410191059f,
    0.19652672111988f,
    0.20433934032917f,
    0.21215195953846f,
    0.21996457874775f,
    0.22777719795704f,
    0.23558983206749f,
    0.24437901377678f,
    0.25633642077446f,
    0.27391481399536f,
    0.29149320721626f,
    0.30907163023949f,
    0.32860317826271f,
    0.34813472628593f,
    0.36766627430916f,
    0.3891509771347f,
    0.41063567996025f,
    0.43212041258812f,
    0.45555827021599f,
    0.47899615764618f,
    0.50877434015274f,
    0.56346267461777f,
    0.62205731868744f,
    0.68065202236176f,
    0.74705934524536f,
    0.81737297773361f,
    0.8994055390358f,
    0.99315708875656f,
    1.212880730629f,
    1.5410155057907f,
};