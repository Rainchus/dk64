#ifndef __ENUMS_H__
#define __ENUMS_H__

typedef enum game_mode_e {
    GAME_MODE_NINTENDO_LOGO,
    GAME_MODE_OPENING_CUTSCENE,
    GAME_MODE_DK_RAP,
    GAME_MODE_DK_TV,
    GAME_MODE_UNKNOWN_4,
    GAME_MODE_MAIN_MENU,
    GAME_MODE_ADVENTURE,
    GAME_MODE_QUIT_GAME,
    GAME_MODE_UNKNOWN_8,
    GAME_MODE_GAME_OVER,
    GAME_MODE_END_SEQUENCE,
    GAME_MODE_DK_THEATRE,
    GAME_MODE_MYSTERY_MENU_MINIGAME,
    GAME_MODE_SNIDES_BONUS_GAME,
    GAME_MODE_END_SEQUENCE_DK_THEATRE
} GameModes;

typedef enum map_e {
    MAP_TEST_MAP,
    MAP_FUNKYS_STORE,
    MAP_DK_ARCADE,
    MAP_KROOL_BARREL_LANKY_MAZE,
    MAP_JAPES_MOUNTAIN,
    MAP_CRANKYS_LAB,
    MAP_JAPES_MINECART,
    MAP_JAPES,
    MAP_JAPES_ARMY_DILLO,
    MAP_JETPAC,
    MAP_KREMLING_KOSH_VERY_EASY,
    MAP_STEALTHY_SNOOP_NORMAL_NO_LOGO,
    MAP_JAPES_SHELL,
    MAP_JAPES_LANKY_CAVE,
    MAP_AZTEC_BEETLE_RACE,
    MAP_SNIDES_HQ,
    MAP_AZTEC_TINY_TEMPLE,
    MAP_HELM,
    MAP_TEETERING_TURTLE_TROUBLE_VERY_EASY,
    MAP_AZTEC_FIVE_DOOR_TEMPLE_DK,
    MAP_AZTEC_LLAMA_TEMPLE,
    MAP_AZTEC_FIVE_DOOR_TEMPLE_DIDDY,
    MAP_AZTEC_FIVE_DOOR_TEMPLE_TINY,
    MAP_AZTEC_FIVE_DOOR_TEMPLE_LANKY,
    MAP_AZTEC_FIVE_DOOR_TEMPLE_CHUNKY,
    MAP_CANDYS_MUSIC_SHOP,
    MAP_FACTORY,
    MAP_FACTORY_CAR_RACE,
    MAP_HELM_LEVEL_INTROS_GAME_OVER,
    MAP_FACTORY_POWER_SHED,
    MAP_GALLEON,
    MAP_GALLEON_KROOLS_SHIP,
    MAP_BATTY_BARREL_BANDIT_EASY,
    MAP_JAPES_CHUNKY_CAVE,
    MAP_DK_ISLES_OVERWORLD,
    MAP_KROOL_BARREL_DK_TARGET_GAME,
    MAP_FACTORY_CRUSHER_ROOM,
    MAP_JAPES_BARREL_BLAST,
    MAP_AZTEC,
    MAP_GALLEON_SEAL_RACE,
    MAP_NINTENDO_LOGO,
    MAP_AZTEC_BARREL_BLAST,
    MAP_TROFF_N_SCOFF,
    MAP_GALLEON_SHIPWRECK_DIDDY_LANKY_CHUNKY,
    MAP_GALLEON_TREASURE_CHEST,
    MAP_GALLEON_MERMAID,
    MAP_GALLEON_SHIPWRECK_DK_TINY,
    MAP_GALLEON_SHIPWRECK_LANKY_TINY,
    MAP_FUNGI,
    MAP_GALLEON_LIGHTHOUSE,
    MAP_KROOL_BARREL_TINY_MUSHROOM_GAME,
    MAP_GALLEON_MECHANICAL_FISH,
    MAP_FUNGI_ANT_HILL,
    MAP_BATTLE_ARENA_BEAVER_BRAWL,
    MAP_GALLEON_BARREL_BLAST,
    MAP_FUNGI_MINECART,
    MAP_FUNGI_DIDDY_BARN,
    MAP_FUNGI_DIDDY_ATTIC,
    MAP_FUNGI_LANKY_ATTIC,
    MAP_FUNGI_DK_BARN,
    MAP_FUNGI_SPIDER,
    MAP_FUNGI_FRONT_PART_OF_MILL,
    MAP_FUNGI_REAR_PART_OF_MILL,
    MAP_FUNGI_MUSHROOM_PUZZLE,
    MAP_FUNGI_GIANT_MUSHROOM,
    MAP_STEALTHY_SNOOP_NORMAL,
    MAP_MAD_MAZE_MAUL_HARD,
    MAP_STASH_SNATCH_NORMAL,
    MAP_MAD_MAZE_MAUL_EASY,
    MAP_MAD_MAZE_MAUL_NORMAL,
    MAP_FUNGI_MUSHROOM_LEAP,
    MAP_FUNGI_SHOOTING_GAME,
    MAP_CAVES,
    MAP_BATTLE_ARENA_KRITTER_KARNAGE,
    MAP_STASH_SNATCH_EASY,
    MAP_STASH_SNATCH_HARD,
    MAP_DK_RAP,
    MAP_MINECART_MAYHEM_EASY,
    MAP_BUSY_BARREL_BARRAGE_EASY,
    MAP_BUSY_BARREL_BARRAGE_NORMAL,
    MAP_MAIN_MENU,
    MAP_TITLE_SCREEN_NOT_FOR_RESALE_VERSION,
    MAP_CAVES_BEETLE_RACE,
    MAP_FUNGI_DOGADON,
    MAP_CAVES_IGLOO_TINY,
    MAP_CAVES_IGLOO_LANKY,
    MAP_CAVES_IGLOO_DK,
    MAP_CASTLE,
    MAP_CASTLE_BALLROOM,
    MAP_CAVES_ROTATING_ROOM,
    MAP_CAVES_SHACK_CHUNKY,
    MAP_CAVES_SHACK_DK,
    MAP_CAVES_SHACK_DIDDY_MIDDLE_PART,
    MAP_CAVES_SHACK_TINY,
    MAP_CAVES_LANKY_HUT,
    MAP_CAVES_IGLOO_CHUNKY,
    MAP_SPLISH_SPLASH_SALVAGE_NORMAL,
    MAP_KLUMSY,
    MAP_CAVES_ICE_CASTLE,
    MAP_SPEEDY_SWING_SORTIE_EASY,
    MAP_CAVES_IGLOO_DIDDY,
    MAP_KRAZY_KONG_KLAMOUR_EASY,
    MAP_BIG_BUG_BASH_VERY_EASY,
    MAP_SEARCHLIGHT_SEEK_VERY_EASY,
    MAP_BEAVER_BOTHER_EASY,
    MAP_CASTLE_TOWER,
    MAP_CASTLE_MINECART,
    MAP_KONG_BATTLE_BATTLE_ARENA,
    MAP_CASTLE_CRYPT_LANKY_TINY,
    MAP_KONG_BATTLE_ARENA_1,
    MAP_FACTORY_BARREL_BLAST,
    MAP_GALLEON_PUFFTOSS,
    MAP_CASTLE_CRYPT_DK_DIDDY_CHUNKY,
    MAP_CASTLE_MUSEUM,
    MAP_CASTLE_LIBRARY,
    MAP_KREMLING_KOSH_EASY,
    MAP_KREMLING_KOSH_NORMAL,
    MAP_KREMLING_KOSH_HARD,
    MAP_TEETERING_TURTLE_TROUBLE_EASY,
    MAP_TEETERING_TURTLE_TROUBLE_NORMAL,
    MAP_TEETERING_TURTLE_TROUBLE_HARD,
    MAP_BATTY_BARREL_BANDIT_EASY_2,
    MAP_BATTY_BARREL_BANDIT_NORMAL,
    MAP_BATTY_BARREL_BANDIT_HARD,
    MAP_MAD_MAZE_MAUL_INSANE,
    MAP_STASH_SNATCH_INSANE,
    MAP_STEALTHY_SNOOP_VERY_EASY,
    MAP_STEALTHY_SNOOP_EASY,
    MAP_STEALTHY_SNOOP_HARD,
    MAP_MINECART_MAYHEM_NORMAL,
    MAP_MINECART_MAYHEM_HARD,
    MAP_BUSY_BARREL_BARRAGE_HARD,
    MAP_SPLISH_SPLASH_SALVAGE_HARD,
    MAP_SPLISH_SPLASH_SALVAGE_EASY,
    MAP_SPEEDY_SWING_SORTIE_NORMAL,
    MAP_SPEEDY_SWING_SORTIE_HARD,
    MAP_BEAVER_BOTHER_NORMAL,
    MAP_BEAVER_BOTHER_HARD,
    MAP_SEARCHLIGHT_SEEK_EASY,
    MAP_SEARCHLIGHT_SEEK_NORMAL,
    MAP_SEARCHLIGHT_SEEK_HARD,
    MAP_KRAZY_KONG_KLAMOUR_NORMAL,
    MAP_KRAZY_KONG_KLAMOUR_HARD,
    MAP_KRAZY_KONG_KLAMOUR_INSANE,
    MAP_PERIL_PATH_PANIC_VERY_EASY,
    MAP_PERIL_PATH_PANIC_EASY,
    MAP_PERIL_PATH_PANIC_NORMAL,
    MAP_PERIL_PATH_PANIC_HARD,
    MAP_BIG_BUG_BASH_EASY,
    MAP_BIG_BUG_BASH_NORMAL,
    MAP_BIG_BUG_BASH_HARD,
    MAP_CASTLE_DUNGEON,
    MAP_HELM_INTRO_STORY,
    MAP_DK_ISLES_DK_THEATRE,
    MAP_FACTORY_MAD_JACK,
    MAP_BATTLE_ARENA_ARENA_AMBUSH,
    MAP_BATTLE_ARENA_MORE_KRITTER_KARNAGE,
    MAP_BATTLE_ARENA_FOREST_FRACAS,
    MAP_BATTLE_ARENA_BISH_BASH_BRAWL,
    MAP_BATTLE_ARENA_KAMIKAZE_KREMLINGS,
    MAP_BATTLE_ARENA_PLINTH_PANIC,
    MAP_BATTLE_ARENA_PINNACLE_PALAVER,
    MAP_BATTLE_ARENA_SHOCKWAVE_SHOWDOWN,
    MAP_CASTLE_BASEMENT,
    MAP_CASTLE_TREE,
    MAP_KROOL_BARREL_DIDDY_KREMLING_GAME,
    MAP_CASTLE_CHUNKY_TOOLSHED,
    MAP_CASTLE_TRASH_CAN,
    MAP_CASTLE_GREENHOUSE,
    MAP_JAPES_LOBBY,
    MAP_HELM_LOBBY,
    MAP_DK_HOUSE,
    MAP_ROCK_INTRO_STORY,
    MAP_AZTEC_LOBBY,
    MAP_GALLEON_LOBBY,
    MAP_FACTORY_LOBBY,
    MAP_TRAINING_GROUNDS,
    MAP_DIVE_BARREL,
    MAP_FUNGI_LOBBY,
    MAP_GALLEON_SUBMARINE,
    MAP_ORANGE_BARREL,
    MAP_BARREL_BARREL,
    MAP_VINE_BARREL,
    MAP_CASTLE_CRYPT,
    MAP_ENGUARDE_ARENA,
    MAP_CASTLE_CAR_RACE,
    MAP_CAVES_BARREL_BLAST,
    MAP_CASTLE_BARREL_BLAST,
    MAP_FUNGI_BARREL_BLAST,
    MAP_FAIRY_ISLAND,
    MAP_KONG_BATTLE_ARENA_2,
    MAP_RAMBI_ARENA,
    MAP_KONG_BATTLE_ARENA_3,
    MAP_CASTLE_LOBBY,
    MAP_CAVES_LOBBY,
    MAP_DK_ISLES_SNIDES_ROOM,
    MAP_CAVES_ARMY_DILLO,
    MAP_AZTEC_DOGADON,
    MAP_TRAINING_GROUNDS_END_SEQUENCE,
    MAP_CASTLE_KING_KUT_OUT,
    MAP_CAVES_SHACK_DIDDY_UPPER_PART,
    MAP_KROOL_BARREL_DIDDY_ROCKETBARREL_GAME,
    MAP_KROOL_BARREL_LANKY_SHOOTING_GAME,
    MAP_KROOL_FIGHT_DK_PHASE,
    MAP_KROOL_FIGHT_DIDDY_PHASE,
    MAP_KROOL_FIGHT_LANKY_PHASE,
    MAP_KROOL_FIGHT_TINY_PHASE,
    MAP_KROOL_FIGHT_CHUNKY_PHASE,
    MAP_BLOOPERS_ENDING,
    MAP_KROOL_BARREL_CHUNKY_HIDDEN_KREMLING_GAME,
    MAP_KROOL_BARREL_TINY_PONY_TAIL_TWIRL_GAME,
    MAP_KROOL_BARREL_CHUNKY_SHOOTING_GAME,
    MAP_KROOL_BARREL_DK_RAMBI_GAME,
    MAP_KLUMSY_ENDING,
    MAP_KROOLS_SHOE,
    MAP_KROOLS_ARENA
} Maps;

typedef enum actors_e {
    ACTOR_UNKNOWN_0,
    ACTOR_UNKNOWN_1,
    ACTOR_DK,
    ACTOR_DIDDY,
    ACTOR_LANKY,
    ACTOR_TINY,
    ACTOR_CHUNKY,
    ACTOR_KRUSHA,
    ACTOR_RAMBI,
    ACTOR_ENGUARDE,
    ACTOR_UNKNOWN_10, // Always loaded, not sure what it is
    ACTOR_UNKNOWN_11, // Always loaded, not sure what it is
    ACTOR_LOADING_ZONE_CONTROLLER, // Always loaded
    ACTOR_PROP_CONTROLLER, // Always loaded
    ACTOR_UNKNOWN_14, // Always loaded, not sure what it is
    ACTOR_UNKNOWN_15, // Always loaded, not sure what it is
    ACTOR_UNKNOWN_16,
    ACTOR_CANNON_BARREL,
    ACTOR_RAMBI_CRATE,
    ACTOR_BARREL_DIDDY_5DI,
    ACTOR_CAMERA_FOCUS_POINT, // Exists during some cutscenes
    ACTOR_PUSHABLE_BOX, // Unused
    ACTOR_BARREL_SPAWNER_UNUSED, // Unused
    ACTOR_CANNON,
    ACTOR_VULTURE_RACE_HOOP,
    ACTOR_HUNKY_CHUNKY_BARREL,
    ACTOR_TNT_BARREL,
    ACTOR_TNT_BARREL_SPAWNER, // Army Dillo
    ACTOR_BONUS_BARREL,
    ACTOR_MINECART,
    ACTOR_BOSS_PROJECTILE_FIREBALL,
    ACTOR_CASTLE_BRIDGE,
    ACTOR_SWINGING_LIGHT,
    ACTOR_VINE_BROWN,
    ACTOR_KREMLING_KOSH_CONTROLLER,
    ACTOR_PROJECTILE_MELON,
    ACTOR_PROJECTILE_PEANUT,
    ACTOR_ROCKETBARREL_ON_KONG,
    ACTOR_PROJECTILE_PINEAPPLE,
    ACTOR_LARGE_BRIDGE, // Unused
    ACTOR_MINI_MONKEY_BARREL,
    ACTOR_PROJECTILE_ORANGE,
    ACTOR_PROJECTILE_GRAPE,
    ACTOR_PROJECTILE_FEATHER,
    ACTOR_BOSS_PROJECTILE_LASER,
    ACTOR_GOLDEN_BANANA, // Vulture, bonus barrels, probably some other places
    ACTOR_TTT_GUN,
    ACTOR_WATERMELON_SLICE,
    ACTOR_PROJECTILE_COCONUTS,
    ACTOR_ROCKETBARREL,
    ACTOR_PROJECTILE_LIME,
    ACTOR_AMMO_CRATE, // Dropped by Red Klaptrap
    ACTOR_ORANGE_PICKUP, // Dropped by Klump & Purple Klaptrap
    ACTOR_BANANA_COIN, // Dropped by "Diddy", otherwise unused?
    ACTOR_DK_COIN, // Minecart
    ACTOR_SMALL_EXPLOSION, // Seasick Chunky
    ACTOR_ORANGSTAND_SPRINT_BARREL,
    ACTOR_STRONG_KONG_BARREL,
    ACTOR_SWINGING_LIGHT_2,
    ACTOR_BOSS_PROJECTILE_FIREBALL_2,
    ACTOR_BANAPORTER,
    ACTOR_BOULDER,
    ACTOR_MINECART_DK,
    ACTOR_VASE_OVAL,
    ACTOR_VASE_DOTS,
    ACTOR_VASE_TRIANGLE,
    ACTOR_VASE_PLUS,
    ACTOR_CANNON_BALL,
    ACTOR_UNKNOWN_68,
    ACTOR_VINE, // Green
    ACTOR_COUNTER, // Unused?
    ACTOR_KREMLING_RED, // Lanky's Keyboard Game in R&D
    ACTOR_BOSS_KEY,
    ACTOR_GALLEON_CANNON, // Galleon Minigame
    ACTOR_GALLEON_CANNON_BALL, // Galleon Minigame Projectile
    ACTOR_BLUEPRINT_DIDDY,
    ACTOR_BLUEPRINT_CHUNKY,
    ACTOR_BLUEPRINT_LANKY,
    ACTOR_BLUEPRINT_DK,
    ACTOR_BLUEPRINT_TINY,
    ACTOR_MINECART_CHUNKY,
    ACTOR_BOSS_FIRE_SPAWNER, // TODO: Verify
    ACTOR_BOULDER_DEBRIS, // Minecart
    ACTOR_SPIDER_WEB, // Fungi miniBoss
    ACTOR_STEEL_KEG_SPAWNER,
    ACTOR_STEEL_KEG,
    ACTOR_CROWN,
    ACTOR_MINECART_BONUS,
    ACTOR_UNKNOWN_88,
    ACTOR_FIRE_UNUSED,
    ACTOR_ICE_WALL,
    ACTOR_BALLOON_DIDDY,
    ACTOR_STALACTITE,
    ACTOR_ROCK_DEBRIS, // Rotating, Unused?
    ACTOR_CAR, // Unused?
    ACTOR_PAUSE_MENU,
    ACTOR_HUNKY_CHUNKY_BARREL_DOGADON,
    ACTOR_TNT_BARREL_SPAWNER_DOGADON,
    ACTOR_TAG_BARREL,
    ACTOR_FIREBALL_GET_OUT, // Get Out
    ACTOR_DIDDY_5DI_PAD_1,
    ACTOR_DIDDY_5DI_PAD_2,
    ACTOR_DIDDY_5DI_PAD_3,
    ACTOR_DIDDY_5DI_PAD_4,
    ACTOR_DIDDY_5DI_PAD_5,
    ACTOR_DIDDY_5DI_PAD_6,
    ACTOR_KONG_REFLECTION,
    ACTOR_BONUS_BARREL_HELM,
    ACTOR_UNKNOWN_108,
    ACTOR_RACE_CHECKPOINT,
    ACTOR_CB_BUNCH, // From Japes rear tunnel boulder
    ACTOR_BALLOON_CHUNKY,
    ACTOR_BALLOON_TINY,
    ACTOR_BALLOON_LANKY,
    ACTOR_BALLOON_DK,
    ACTOR_KLUMSY_CAGE, // Also rabbit race finish line for an unknown reason
    ACTOR_CHAIN,
    ACTOR_BEANSTALK,
    ACTOR_MULTIPLAYER_QMARK_YELLOW,
    ACTOR_MULTIPLAYER_BANANA_BLUE,
    ACTOR_MULITPLAYER_BANANA_YELLOW,
    ACTOR_MULTIPLAYER_CRYSTAL,
    ACTOR_MULTIPLAYER_RACECOIN,
    ACTOR_REFLECTION_MUSEUM,
    ACTOR_BARRELGUN_PERILPATHPANIC,
    ACTOR_BARRELGUN_KRAZYKONGKLAMOUR,
    ACTOR_FLYSWATTER,
    ACTOR_SEARCHLIGHT,
    ACTOR_HEADPHONES,
    ACTOR_CRATE_ENGUARDE,
    ACTOR_APPLE,
    ACTOR_WORM,
    ACTOR_CRATE_ENGUARDE_0,
    ACTOR_BARREL,
    ACTOR_BONUS_TRAINING, // Training Barrel
    ACTOR_BOOMBOX,
    ACTOR_TAGBARREL_0,
    ACTOR_TAGBARREL_TNS,
    ACTOR_B_LOCKER,
    ACTOR_RAINBOW_COIN_PATCH,
    ACTOR_RAINBOW_COIN,
    ACTOR_UNKNOWN_141,
    ACTOR_UNKNOWN_142,
    ACTOR_UNKNOWN_143,
    ACTOR_UNKNOWN_144,
    ACTOR_CANNON_SEASICK,
    ACTOR_UNKNOWN_146,
    ACTOR_BALLOON_KROOL,
    ACTOR_ROPE,
    ACTOR_BARREL_BANANA,
    ACTOR_BARREL_BANANA_SPAWNER,
    ACTOR_UNKNOWN_151,
    ACTOR_UNKNOWN_152,
    ACTOR_UNKNOWN_153,
    ACTOR_UNKNOWN_154,
    ACTOR_UNKNOWN_155,
    ACTOR_WRINKLY,
    ACTOR_UNKNOWN_157,
    ACTOR_UNKNOWN_158,
    ACTOR_UNKNOWN_159,
    ACTOR_UNKNOWN_160,
    ACTOR_UNKNOWN_161,
    ACTOR_UNKNOWN_162,
    ACTOR_FAIRY_QUEEN,
    ACTOR_TOMATO_ICE,
    ACTOR_BOSS_KUTOUT_TAG,
    ACTOR_BOSS_KUTOUT_SEGMENT,
    ACTOR_CANNON_0,
    ACTOR_UNKNOWN_168,
    ACTOR_PUFFTUP,
    ACTOR_BOSS_KROOL_GLOVE,
    ACTOR_PROJECTILE_ORANGE_KRUSHA,
    ACTOR_UNKNOWN_172,
    ACTOR_CUTSCENE_CONTROLLER,
    ACTOR_UNKNOWN_174,
    ACTOR_KABOOM,
    ACTOR_TIMER,
    ACTOR_TIMER_CONTROLLER,
    ACTOR_BEAVER_BLUE,
    ACTOR_SHOCKWAVE_MAD_JACK,
    ACTOR_KRASH,
    ACTOR_BOOK,
    ACTOR_KLOBBER,
    ACTOR_ZINGER_0,
    ACTOR_SNIDE,
    ACTOR_BOSS_ARMY_DILLO,
    ACTOR_MINIGAME_KREMLING,
    ACTOR_KLUMP,
    ACTOR_CAMERA,
    ACTOR_CRANKY,
    ACTOR_FUNKY,
    ACTOR_CANDY,
    ACTOR_BEETLE,
    ACTOR_MERMAID,
    ACTOR_VULTURE_SHOOTING,
    ACTOR_SQUAWKS,
    ACTOR_CUTSCENE_DK,
    ACTOR_CUTSCENE_DIDDY,
    ACTOR_CUTSCENE_LANKY,
    ACTOR_CUTSCENE_TINY,
    ACTOR_CUTSCENE_CHUNKY,
    ACTOR_LLAMA,
    ACTOR_PICTURE,
    ACTOR_PADLOCK_TNS,
    ACTOR_BOSS_MAD_JACK,
    ACTOR_KLAPTRAP_GREEN,
    ACTOR_ZINGER_1,
    ACTOR_VULTURE_RACE,
    ACTOR_KLAPTRAP_PURPLE,
    ACTOR_KLAPTRAP_RED,
    ACTOR_CONTROLLER_GETOUT,
    ACTOR_KLAPTRAP_SKELETON,
    ACTOR_BEAVER_GOLD,
    ACTOR_FIRE_COLUMN_SPAWNER,
    ACTOR_MINECART_TNT_MINIGAME,
    ACTOR_MINECART_TNT_COURSE,
    ACTOR_BOSS_PUFFTOSS,
    ACTOR_UNKNOWN_217,
    ACTOR_BANDIT_HANDLE,
    ACTOR_BANDIT_SLOT,
    ACTOR_CANNON_SEASICK_0,
    ACTOR_BOSS_KROOL_LIGHT,
    ACTOR_BOSS_KROOL_PEEL,
    ACTOR_FIREBALL_SPAWNER,
    ACTOR_MUSHROOM_MAN,
    ACTOR_UNKNOWN_225,
    ACTOR_TROFF,
    ACTOR_BOSS_KROOL_FOOT,
    ACTOR_TOY_MONSTER,
    ACTOR_BOSS_KROOL_TOE,
    ACTOR_RULER,
    ACTOR_TOY_BOX,
    ACTOR_TEXT_OVERLAY,
    ACTOR_SQUAWKS_0,
    ACTOR_SCOFF,
    ACTOR_ROBO_KREMLING,
    ACTOR_BOSS_DOGADON,
    ACTOR_KROSSBONES_HEAD,
    ACTOR_KREMLING,
    ACTOR_BONGOS,
    ACTOR_SPOTLIGHT_FISH,
    ACTOR_KASPLAT_DK,
    ACTOR_KASPLAT_DIDDY,
    ACTOR_KASPLAT_LANKY,
    ACTOR_KASPLAT_TINY,
    ACTOR_KASPLAT_CHUNKY,
    ACTOR_MECHANICAL_FISH,
    ACTOR_SEAL,
    ACTOR_FAIRY,
    ACTOR_SPOTLIGHT_SQUAWKS,
    ACTOR_OWL,
    ACTOR_BOSS_SPIDER,
    ACTOR_RABBIT_RACE,
    ACTOR_NINTENDO_LOGO,
    ACTOR_CUTSCENE_OBJECT,
    ACTOR_SHOCKWAVE,
    ACTOR_MINIGAME_CONTROLLER,
    ACTOR_FIRE_BREATH_SPAWNER,
    ACTOR_BOSS_SHOCKWAVE,
    ACTOR_GUARD,
    ACTOR_BOSS_KROOL_OVERLAY,
    ACTOR_ROBO_ZINGER,
    ACTOR_KROSSBONES,
    ACTOR_BOSS_DOGADON_SHOCKWAVE,
    ACTOR_SQUAWKS_1,
    ACTOR_BOSS_DOGADON_LIGHTBEAM,
    ACTOR_DK_RAP_CONTROLLER,
    ACTOR_SHURI,
    ACTOR_GIMPFISH,
    ACTOR_MR_DICE_0,
    ACTOR_SIR_DOMINO,
    ACTOR_MR_DICE_1,
    ACTOR_RABBIT_CAVES,
    ACTOR_FIREBALL_WITH_GLASSES,
    ACTOR_UNKNOWN_274,
    ACTOR_KLUMSY,
    ACTOR_SPIDERLING,
    ACTOR_SQUAWKS_2,
    ACTOR_PROJECTILE_SPIDER,
    ACTOR_TRAP_BUBBLE,
    ACTOR_SILK,
    ACTOR_BOSS_KROOL_DK,
    ACTOR_MINIGAME_BOTHER_KLAPTRAP,
    ACTOR_SKELETON_HEAD,
    ACTOR_UNKNOWN_284,
    ACTOR_BAT,
    ACTOR_CLAM,
    ACTOR_UNKNOWN_287,
    ACTOR_TOMATO_FUNGI,
    ACTOR_KRITTER_IN_A_SHEET,
    ACTOR_PUFFTUP_0,
    ACTOR_KOSHA,
    ACTOR_BOSS_KROOL_DIDDY,
    ACTOR_BOSS_KROOL_LANKY,
    ACTOR_BOSS_KROOL_TINY,
    ACTOR_BOSS_KROOL_CHUNKY,
    ACTOR_UNKNOWN_296,
    ACTOR_BATTLE_CROWN_CONTROLLER,
    ACTOR_UNKNOWN_298,
    ACTOR_TEXT_BUBBLE,
    ACTOR_SNAKE,
    ACTOR_TURTLE,
    ACTOR_CAR_FACTORY_PLAYER,
    ACTOR_CAR_FACTORY_ENEMY,
    ACTOR_CAMERA_CARRACE,
    ACTOR_MISSILE_CAR,
    ACTOR_UNKNOWN_306,
    ACTOR_UNKNOWN_307,
    ACTOR_SEAL_0,
    ACTOR_INSTRUMENT_LOGO,
    ACTOR_SPOTLIGHT,
    ACTOR_RACE_CHECKPOINT_0,
    ACTOR_MINECART_TNT_0,
    ACTOR_PARTICLE_IDLE,
    ACTOR_RAREWARE_LOGO,
    ACTOR_UNKNOWN_315,
    ACTOR_TAGBARREL_KONG,
    ACTOR_TAGBARREL_LOCKED,
    ACTOR_UNKNOWN_318,
    ACTOR_PROPELLER,
    ACTOR_POTION,
    ACTOR_FAIRY_REFILL,
    ACTOR_CAR_CASTLE_PLAYER,
    ACTOR_CAR_CASTLE_ENEMY,
    ACTOR_PURCHASE_TEXT_OVERLAY,
    ACTOR_SHOCKWAVE_SLAM,
    ACTOR_MAIN_MENU_CONTROLLER,
    ACTOR_MINIGAME_KRAZYKONGKLAMOUR_KONG,
    ACTOR_MINIGAME_PERILPATHPANIC_KLAPTRAP,
    ACTOR_MINIGAME_PERILPATHPANIC_FAIRY,
    ACTOR_MINIGAME_BIGBUGBASH_BUG,
    ACTOR_MINIGAME_SEARCHLIGHTSEEK_KLAPTRAP,
    ACTOR_FLYSWATTER_SHADOW,
    ACTOR_BARREL_MAIN_MENU,
    ACTOR_PADLOCK_KLUMSY,
    ACTOR_MENU_SNIDE,
    ACTOR_TRAINING_BARREL_CONTROLLER,
    ACTOR_MAIN_MENU_MULTIPLAYER_KONG,
    ACTOR_END_SEQUENCE_CONTROLLER,
    ACTOR_ARENA_CONTROLLER,
    ACTOR_BUG,
    ACTOR_UNKNOWN_341,
    ACTOR_TRY_AGAIN_DIALOG,
    ACTOR_PAUSE_MENU_MYSTERY_MENU
} Actors;

typedef enum sfx_e{
    SFX_0_SILENCE,
    
    SFX_40_ARCADE_JUMPMAN_MOVING = 0x40,
    SFX_41_ARCADE_JUMPMAN_JUMPING,
    SFX_42_ARCADE_JUMPMAN_HIT,
    SFX_43_ARCADE_SPRING_SPRINGING,
    SFX_44_ARCADE_SPRING_FALL,
    SFX_45_ARCADE_POINTS,
    SFX_46_ARCADE_INTRO,

    SFX_48_ARCADE_RUNNING_OUT_OF_TIME = 0x48,
    SFX_49_ARCADE_HAMMER_MUSIC,

    SFX_4B_ARCADE_HAMMER_ATTACK = 0x4b,
    SFX_4C_ARCADE_PAULINE_SAVED_STAGE,
    SFX_4D_ARCADE_25M,
    SFX_4E_ARCADE_100M,
    SFX_4F_ARCADE_50M,

    SFX_53_ARCADE_DK_GRUNT = 0x53,
    SFX_54_ARCADE_JUMPMAN_DEATH
} SFX_E;

typedef enum music_e {
    MUSIC_0_SILENCE,
    MUSIC_1_JUNGLE_JAPES_STARTING_AREA,
    MUSIC_2_CRANKYS_LAB,
    MUSIC_3_JUNGLE_JAPES_MINECART,
    MUSIC_4_JUNGLE_JAPES_ARMY_DILLO,
    MUSIC_5_JUNGLE_JAPES_CAVES_UNDERGROUND,
    MUSIC_6_FUNKYS_HUT,
    MUSIC_7_UNUSED_COIN_PICKUP,
    MUSIC_8_BONUS_MINIGAMES,
    MUSIC_9_TRIANGLE_TRAMPLE,
    MUSIC_10_GUITAR_GAZUMP,
    MUSIC_11_BONGO_BLAST,
    MUSIC_12_TROMBONE_TREMOR,
    MUSIC_13_SAXOPHONE_SLAM,
    MUSIC_14_ANGRY_AZTEC,
    MUSIC_15_TRANSFORMATION,
    MUSIC_16_MINI_MONKEY,
    MUSIC_17_HUNKY_CHUNKY,
    MUSIC_18_GB_KEY_GET,
    MUSIC_19_ANGRY_AZTEC_BEETLE_SLIDE,
    MUSIC_20_OH_BANANA,
    MUSIC_21_ANGRY_AZTEC_TEMPLE,
    MUSIC_22_COMPANY_COIN_GET,
    MUSIC_23_BANANA_COIN_GET,
    MUSIC_24_GOING_THROUGH_VULTURE_RING,
    MUSIC_25_ANGRY_AZTEC_DOGADON,
    MUSIC_26_ANGRY_AZTEC_5DT,
    MUSIC_27_FRANTIC_FACTORY_CAR_RACE,
    MUSIC_28_FRANTIC_FACTORY,
    MUSIC_29_SNIDES_HQ,
    MUSIC_30_JUNGLE_JAPES_TUNNELS,
    MUSIC_31_CANDYS_MUSIC_SHOP,
    MUSIC_32_MINECART_COIN_GET,
    MUSIC_33_MELON_SLICE_GET,
    MUSIC_34_PAUSE_MENU,
    MUSIC_35_CRYSTAL_COCONUT_GET,
    MUSIC_36_RAMBI,
    MUSIC_37_ANGRY_AZTEC_TUNNELS,
    MUSIC_38_WATER_DROPLETS,
    MUSIC_39_FRANTIC_FACTORY_MAD_JACK,
    MUSIC_40_SUCCESS,
    MUSIC_41_START_TO_PAUSE_GAME,
    MUSIC_42_FAILURE,
    MUSIC_43_DK_TRANSITION_OPENING,
    MUSIC_44_DK_TRANSITION_CLOSING,
    MUSIC_45_UNUSED_HIGH_PITCHED_JAPES,
    MUSIC_46_FAIRY_TICK,
    MUSIC_47_MELON_SLICE_DROP,
    MUSIC_48_ANGRY_AZTEC_CHUNKY_KLAPTRAPS,
    MUSIC_49_FRANTIC_FACTORY_CRUSHER_ROOM,
    MUSIC_50_JUNGLE_JAPES_BABOON_BLAST,
    MUSIC_51_FRANTIC_FACTORY_RND,
    MUSIC_52_FRANTIC_FACTORY_PRODUCTION_ROOM,
    MUSIC_53_TROFF_N_SCOFF,
    MUSIC_54_BOSS_DEFEAT,
    MUSIC_55_ANGRY_AZTEC_BABOON_BLAST,
    MUSIC_56_GLOOMY_GALLEON_OUTSIDE,
    MUSIC_57_BOSS_UNLOCK,
    MUSIC_58_AWAITING_ENTERING_THE_BOSS,
    MUSIC_59_GENERIC_TWINKLY_SOUNDS,
    MUSIC_60_GLOOMY_GALLEON_PUFFTOSS,
    MUSIC_61_GLOOMY_GALLEON_SEAL_RACE,
    MUSIC_62_GLOOMY_GALLEON_TUNNELS,
    MUSIC_63_GLOOMY_GALLEON_LIGHTHOUSE,
    MUSIC_64_BATTLE_ARENA,
    MUSIC_65_DROP_COINS_MINECART,
    MUSIC_66_FAIRY_NEARBY,
    MUSIC_67_CHECKPOINT,
    MUSIC_68_FUNGI_FOREST_DAY,
    MUSIC_69_BLUEPRINT_GET,
    MUSIC_70_FUNGI_FOREST_NIGHT,
    MUSIC_71_STRONG_KONG,
    MUSIC_72_ROCKETBARREL_BOOST,
    MUSIC_73_ORANGSTAND_SPRINT,
    MUSIC_74_FUNGI_FOREST_MINECART,
    MUSIC_75_DK_RAP,
    MUSIC_76_BLUEPRINT_DROP,
    MUSIC_77_GLOOMY_GALLEON_2DS,
    MUSIC_78_GLOOMY_GALLEON_5DS_SUBMARINE,
    MUSIC_79_GLOOMY_GALLEON_PEARLS_CHEST,
    MUSIC_80_GLOOMY_GALLEON_MERMAID_PALACE,
    MUSIC_81_FUNGI_FOREST_DOGADON,
    MUSIC_82_MAD_MAZE_MAUL,
    MUSIC_83_CRYSTAL_CAVES,
    MUSIC_84_CRYSTAL_CAVES_GIANT_KOSHA_TANTRUM,
    MUSIC_85_NINTENDO_LOGO_OLD,
    MUSIC_86_SUCCESS_RACES,
    MUSIC_87_FAILURE_RACES_TRY_AGAIN,
    MUSIC_88_BONUS_BARREL_INTRODUCTION,
    MUSIC_89_STEALTHY_SNOOP,
    MUSIC_90_MINECART_MAYHEM,
    MUSIC_91_GLOOMY_GALLEON_MECHANICAL_FISH,
    MUSIC_92_GLOOMY_GALLEON_BABOON_BLAST,
    MUSIC_93_FUNGI_FOREST_ANTHILL,
    MUSIC_94_FUNGI_FOREST_BARN,
    MUSIC_95_FUNGI_FOREST_MILL,
    MUSIC_96_GENERIC_SEASIDE_SOUNDS,
    MUSIC_97_FUNGI_FOREST_SPIDER,
    MUSIC_98_FUNGI_FOREST_MUSHROOM_TOP_ROOMS,
    MUSIC_99_FUNGI_FOREST_GIANT_MUSHROOM,
    MUSIC_100_BOSS_INTRODUCTION,
    MUSIC_101_TAG_BARREL_ALL_OF_THEM,
    MUSIC_102_CRYSTAL_CAVES_BEETLE_RACE,
    MUSIC_103_CRYSTAL_CAVES_IGLOOS,
    MUSIC_104_MINI_BOSS,
    MUSIC_105_CREEPY_CASTLE,
    MUSIC_106_CREEPY_CASTLE_MINECART,
    MUSIC_107_BABOON_BALLOON,
    MUSIC_108_GORILLA_GONE,
    MUSIC_109_DK_ISLES,
    MUSIC_110_DK_ISLES_K_ROOLS_SHIP,
    MUSIC_111_DK_ISLES_BANANA_FAIRY_ISLAND,
    MUSIC_112_DK_ISLES_K_LUMSYS_PRISON,
    MUSIC_113_HIDEOUT_HELM_BLAST_O_MATIC_ON,
    MUSIC_114_MOVE_GET,
    MUSIC_115_GUN_GET,
    MUSIC_116_HIDEOUT_HELM_BLAST_O_MATIC_OFF,
    MUSIC_117_HIDEOUT_HELM_BONUS_BARRELS,
    MUSIC_118_CRYSTAL_CAVES_CABINS,
    MUSIC_119_CRYSTAL_CAVES_ROTATING_ROOM,
    MUSIC_120_CRYSTAL_CAVES_TILE_FLIPPING,
    MUSIC_121_CREEPY_CASTLE_TUNNELS,
    MUSIC_122_INTRO_STORY_MEDLEY,
    MUSIC_123_TRAINING_GROUNDS,
    MUSIC_124_ENGUARDE,
    MUSIC_125_K_LUMSY_CELEBRATION,
    MUSIC_126_CREEPY_CASTLE_CRYPT,
    MUSIC_127_HEADPHONES_GET,
    MUSIC_128_PEARL_GET,
    MUSIC_129_CREEPY_CASTLE_DUNGEON_W_CHAINS,
    MUSIC_130_ANGRY_AZTEC_LOBBY,
    MUSIC_131_JUNGLE_JAPES_LOBBY,
    MUSIC_132_FRANTIC_FACTORY_LOBBY,
    MUSIC_133_GLOOMY_GALLEON_LOBBY,
    MUSIC_134_MAIN_MENU,
    MUSIC_135_CREEPY_CASTLE_INNER_CRYPTS,
    MUSIC_136_CREEPY_CASTLE_BALLROOM,
    MUSIC_137_CREEPY_CASTLE_GREENHOUSE,
    MUSIC_138_K_ROOLS_THEME,
    MUSIC_139_CREEPY_CASTLE_SHED,
    MUSIC_140_CREEPY_CASTLE_WIND_TOWER,
    MUSIC_141_CREEPY_CASTLE_TREE,
    MUSIC_142_CREEPY_CASTLE_MUSEUM,
    MUSIC_143_BBLAST_FINAL_STAR,
    MUSIC_144_DROP_RAINBOW_COIN,
    MUSIC_145_RAINBOW_COIN_GET,
    MUSIC_146_NORMAL_STAR,
    MUSIC_147_BEAN_GET,
    MUSIC_148_CRYSTAL_CAVES_ARMY_DILLO,
    MUSIC_149_CREEPY_CASTLE_KUT_OUT,
    MUSIC_150_CREEPY_CASTLE_DUNGEON_W_OUT_CHAINS,
    MUSIC_151_BANANA_MEDAL_GET,
    MUSIC_152_K_ROOLS_BATTLE,
    MUSIC_153_FUNGI_FOREST_LOBBY,
    MUSIC_154_CRYSTAL_CAVES_LOBBY,
    MUSIC_155_CREEPY_CASTLE_LOBBY,
    MUSIC_156_HIDEOUT_HELM_LOBBY,
    MUSIC_157_CREEPY_CASTLE_TRASH_CAN,
    MUSIC_158_END_SEQUENCE,
    MUSIC_159_K_LUMSY_ENDING,
    MUSIC_160_JUNGLE_JAPES,
    MUSIC_161_JUNGLE_JAPES_CRANKYS_AREA,
    MUSIC_162_K_ROOL_TAKEOFF,
    MUSIC_163_CRYSTAL_CAVES_BABOON_BLAST,
    MUSIC_164_FUNGI_FOREST_BABOON_BLAST,
    MUSIC_165_CREEPY_CASTLE_BABOON_BLAST,
    MUSIC_166_DK_ISLES_SNIDES_ROOM,
    MUSIC_167_K_ROOLS_ENTRANCE,
    MUSIC_168_MONKEY_SMASH,
    MUSIC_169_FUNGI_FOREST_RABBIT_RACE,
    MUSIC_170_GAME_OVER,
    MUSIC_171_WRINKLY_KONG,
    MUSIC_172_100TH_CB_GET,
    MUSIC_173_K_ROOLS_DEFEAT,
    MUSIC_174_NINTENDO_LOGO
} MUSIC_E;

typedef enum flagtype_e {
    FLAG_TYPE_PERMANENT,
    FLAG_TYPE_GLOBAL,
    FLAG_TYPE_TEMPORARY
} FlagTypes;

typedef enum permflag_e {
    // Japes
    PERMFLAG_PROGRESS_JAPES_FIRST_GATE_OPENED = 0x0,
    PERMFLAG_ITEM_GB_JAPES_FRONT_OF_DIDDY_CAGE = 0x4,
    PERMFLAG_KONG_DIDDY = 0x6,
    PERMFLAG_ITEM_GB_JAPES_PAINTING = 0xA,
    PERMFLAG_ITEM_KEY_1 = 0x1A,
    PERMFLAG_CUTSCENE_JAPES_FTCS = 0x1B,
    PERMFLAG_CUTSCENE_DIDDY_HELP_ME = 0x2A,
    // Aztec
    PERMFLAG_PROGRESS_LLAMA_FREE = 0x32,
    PERMFLAG_KONG_TINY = 0x42,
    PERMFLAG_KONG_LANKY = 0x46,
    PERMFLAG_ITEM_KEY_2 = 0x4A,
    PERMFLAG_PROGRESS_LLAMA_TEMPLE_WATER_COOLED = 0x4C,
    PERMFLAG_CUTSCENE_LLAMA_FTCS = 0x5C,
    PERMFLAG_CUTSCENE_LANKY_HELP_ME = 0x5D,
    PERMFLAG_CUTSCENE_TINY_HELP_ME = 0x5E,
    PERMFLAG_CUTSCENE_AZTEC_FTCS = 0x5F,
    // Factory
    PERMFLAG_KONG_CHUNKY = 0x75,
    PERMFLAG_ITEM_GB_FACTORY_ARCADE = 0x82,
    PERMFLAG_PROGRESS_ARCADE_2_COINS_PAID = 0x83,
    PERMFLAG_ITEM_NINTENDO_COIN = 0x84,
    PERMFLAG_ITEM_KEY_3 = 0x8A,
    PERMFLAG_CUTSCENE_CHUNKY_HELP_ME = 0x8C,
    // Galleon
    PERMFLAG_PROGRESS_SEAL_FREED = 0x9E,
    PERMFLAG_PROGRESS_IS_GALLEON_WATER_RAISED = 0xA0,
    PERMFLAG_ITEM_GB_GALLEON_SEALRACE = 0xA5,
    PERMFLAG_ITEM_KEY_4 = 0xA8,
    PERMFLAG_ITEM_PEARL_1 = 0xBA,
    PERMFLAG_ITEM_PEARL_2 = 0xBB,
    PERMFLAG_ITEM_PEARL_3 = 0xBC,
    PERMFLAG_ITEM_PEARL_4 = 0xBD,
    PERMFLAG_ITEM_PEARL_5 = 0xBE,
    PERMFLAG_ITEM_GB_GALLEON_MERMAID = 0xBF,
    PERMFLAG_ITEM_GB_GALLEON_SEALFREE = 0xC1,
    PERMFLAG_CUTSCENE_GALLEON_FTCS = 0xC2,
    // Fungi
    PERMFLAG_PROGRESS_IS_NIGHTTIME = 0xCE,
    PERMFLAG_PROGRESS_CANNON_COCONUT_SWITCH = 0xE6,
    PERMFLAG_PROGRESS_CANNON_GRAPE_SWITCH = 0xE7,
    PERMFLAG_PROGRESS_CANNON_FEATHER_SWITCH = 0xE8,
    PERMFLAG_PROGRESS_CANNON_PEANUT_SWITCH = 0xE9,
    PERMFLAG_PROGRESS_CANNON_PINEAPPLE_SWITCH = 0xEA,
    PERMFLAG_ITEM_KEY_5 = 0xEC,
    PERMFLAG_ITEM_GB_FUNGI_SPIDER_BOSS = 0xF7,
    PERMFLAG_PROGRESS_RABBIT_RACE_1_COMPLETE = 0xF8,
    PERMFLAG_PROGRESS_BEANSTALK_GROWN = 0xFB,
    PERMFLAG_ITEM_GB_FUNGI_RABBIT_RACE = 0xF9,
    PERMFLAG_ITEM_GB_FUNGI_APPLE = 0xFD,
    PERMFLAG_CUTSCENE_FUNGI_FTCS = 0x101,
    // Caves
    PERMFLAG_ITEM_GB_CAVES_LANKY_CABIN = 0x108,
    PERMFLAG_ITEM_GB_CAVES_ICE_TOMATO = 0x10F,
    PERMFLAG_ITEM_GB_CAVES_DIDDY_IGLOO = 0x112,
    PERMFLAG_CUTSCENE_ROTATING_ROOM_FTCS = 0x115,
    PERMFLAG_ITEM_GB_CAVES_CHUNKY_IGLOO = 0x116,
    PERMFLAG_CUTSCENE_CAVES_FTCS = 0x11A,
    PERMFLAG_ITEM_KEY_6 = 0x124,
    PERMFLAG_ITEM_ISLES_RAREWARE_GB = 0x12D, // What's this doing here?
    PERMFLAG_CUTSCENE_DK_IGLOO_FTCS = 0x12F,
    // Castle
    PERMFLAG_ITEM_KEY_7 = 0x13D,
    PERMFLAG_ITEM_GB_CASTLE_GREENHOUSE = 0x143,
    PERMFLAG_ITEM_GB_CASTLE_CAR_RACE = 0x145,
    PERMFLAG_ITEM_GB_CASTLE_FACE_PUZZLE = 0x146,
    PERMFLAG_CUTSCENE_CASTLE_FTCS = 0x15D,
    PERMFLAG_ITEM_GB_CASTLE_TRASH_CAN = 0x15F,
    //
    PERMFLAG_CUTSCENE_TROFF_N_SCOFF_FTCS = 0x167,
    PERMFLAG_CUTSCENE_MINI_MONKEY_FTCS = 0x168,
    PERMFLAG_CUTSCENE_HUNKY_CHUNKY_FTCS = 0x169,
    PERMFLAG_CUTSCENE_ORANGSTAND_SPRINT_FTCS = 0x16A,
    PERMFLAG_CUTSCENE_STRONG_KONG_FTCS = 0x16B,
    PERMFLAG_CUTSCENE_RAINBOW_COIN_FTCS = 0x16C,
    PERMFLAG_CUTSCENE_RAMBI_FTCS = 0x16D,
    PERMFLAG_CUTSCENE_ENGUARDE_FTCS = 0x16E,
    PERMFLAG_PROGRESS_FIRST_ORANGE_COLLECTED = 0x173,
    PERMFLAG_CUTSCENE_SNIDE_FTCS = 0x174,
    PERMFLAG_CUTSCENE_CANDY_FTCS = 0x175,
    PERMFLAG_CUTSCENE_FUNKY_FTCS = 0x176,
    PERMFLAG_CUTSCENE_CRANKY_FTCS = 0x177,
    PERMFLAG_FTT_WRINKLY = 0x178,
    PERMFLAG_ITEM_MOVE_SHOCKWAVE_CAMERA = 0x179,
    PERMFLAG_CUTSCENE_TRAINING_GROUNDS_WATERFALL = 0x17A,
    // Helm
    PERMFLAG_ITEM_RAREWARE_COIN = 0x17B,
    PERMFLAG_ITEM_KEY_8 = 0x17C,
    //
    PERMFLAG_FTT_B_LOCKER = 0x17E,
    PERMFLAG_PROGRESS_TRAINING_SPAWNED = 0x17F,
    PERMFLAG_PROGRESS_GIVEN_FIRST_SLAM = 0x180,
    PERMFLAG_ITEM_MOVE_DIVING = 0x182,
    PERMFLAG_ITEM_MOVE_VINES = 0x183,
    PERMFLAG_ITEM_MOVE_ORANGETHROWING = 0x184,
    PERMFLAG_ITEM_MOVE_BARRELTHROWING = 0x185,
    PERMFLAG_CUTSCENE_ISLES_FTCS = 0x186,
    PERMFLAG_PROGRESS_ALL_TRAINING_COMPLETE = 0x187,
    PERMFLAG_PROGRESS_RAREWARE_ROOM_OPEN = 0x189,
    PERMFLAG_UNK_18A = 0x18A,
    PERMFLAG_PROGRESS_FIRST_AMMO_OR_CB_OR_BUNCH_COLLECTED = 0x18B,
    PERMFLAG_PROGRESS_FIRST_COIN_COLLECTED = 0x18C,
    PERMFLAG_PROGRESS_K_ROOL_DEFEATED = 0x1B0,
    PERMFLAG_PROGRESS_JAPES_LOBBY_OPEN = 0x1BB,
    // Key turn ins
    PERMFLAG_PROGRESS_KEY_1_TURNED = 0x1BC,
    PERMFLAG_PROGRESS_KEY_2_TURNED = 0x1BD,
    PERMFLAG_PROGRESS_KEY_3_TURNED = 0x1BE,
    PERMFLAG_PROGRESS_KEY_4_TURNED = 0x1BF,
    PERMFLAG_PROGRESS_KEY_5_TURNED = 0x1C0,
    PERMFLAG_PROGRESS_KEY_6_TURNED = 0x1C1,
    PERMFLAG_PROGRESS_KEY_7_TURNED = 0x1C2,
    PERMFLAG_PROGRESS_KEY_8_TURNED = 0x1C3,

    PERMFLAG_ITEM_GB_NULL = 0x1C4, // Bonus Barrel that's not in the game at all, but has data. Spawns a kongless GB
    // Levels Entered
    PERMFLAG_LEVEL_ENTERED_JAPES = 0x1C5,
    PERMFLAG_LEVEL_ENTERED_AZTEC = 0x1C6,
    PERMFLAG_LEVEL_ENTERED_FACTORY = 0x1C7,
    PERMFLAG_LEVEL_ENTERED_GALLEON = 0x1C8,
    PERMFLAG_LEVEL_ENTERED_FUNGI = 0x1C9,
    PERMFLAG_LEVEL_ENTERED_CAVES = 0x1CA,
    PERMFLAG_LEVEL_ENTERED_CASTLE = 0x1CB,
    PERMFLAG_LEVEL_ENTERED_HELM = 0x1CC,
    // B Lockers Cleared
    PERMFLAG_B_LOCKER_CLEARED_JAPES = 0x1CD,
    PERMFLAG_B_LOCKER_CLEARED_AZTEC = 0x1CE,
    PERMFLAG_B_LOCKER_CLEARED_FACTORY = 0x1CF,
    PERMFLAG_B_LOCKER_CLEARED_GALLEON = 0x1D0,
    PERMFLAG_B_LOCKER_CLEARED_FUNGI = 0x1D1,
    PERMFLAG_B_LOCKER_CLEARED_CAVES = 0x1D2,
    PERMFLAG_B_LOCKER_CLEARED_CASTLE = 0x1D3,
    PERMFLAG_B_LOCKER_CLEARED_HELM = 0x1D4,
    // Blueprints
    PERMFLAG_ITEM_BLUEPRINT_JAPES_DK = 0x1D5,
    PERMFLAG_ITEM_BLUEPRINT_JAPES_DIDDY,
    PERMFLAG_ITEM_BLUEPRINT_JAPES_LANKY,
    PERMFLAG_ITEM_BLUEPRINT_JAPES_TINY,
    PERMFLAG_ITEM_BLUEPRINT_JAPES_CHUNKY,
    PERMFLAG_ITEM_BLUEPRINT_AZTEC_DK,
    PERMFLAG_ITEM_BLUEPRINT_AZTEC_DIDDY,
    PERMFLAG_ITEM_BLUEPRINT_AZTEC_LANKY,
    PERMFLAG_ITEM_BLUEPRINT_AZTEC_TINY,
    PERMFLAG_ITEM_BLUEPRINT_AZTEC_CHUNKY,
    PERMFLAG_ITEM_BLUEPRINT_FACTORY_DK,
    PERMFLAG_ITEM_BLUEPRINT_FACTORY_DIDDY,
    PERMFLAG_ITEM_BLUEPRINT_FACTORY_LANKY,
    PERMFLAG_ITEM_BLUEPRINT_FACTORY_TINY,
    PERMFLAG_ITEM_BLUEPRINT_FACTORY_CHUNKY,
    PERMFLAG_ITEM_BLUEPRINT_GALLEON_DK,
    PERMFLAG_ITEM_BLUEPRINT_GALLEON_DIDDY,
    PERMFLAG_ITEM_BLUEPRINT_GALLEON_LANKY,
    PERMFLAG_ITEM_BLUEPRINT_GALLEON_TINY,
    PERMFLAG_ITEM_BLUEPRINT_GALLEON_CHUNKY,
    PERMFLAG_ITEM_BLUEPRINT_FUNGI_DK,
    PERMFLAG_ITEM_BLUEPRINT_FUNGI_DIDDY,
    PERMFLAG_ITEM_BLUEPRINT_FUNGI_LANKY,
    PERMFLAG_ITEM_BLUEPRINT_FUNGI_TINY,
    PERMFLAG_ITEM_BLUEPRINT_FUNGI_CHUNKY,
    PERMFLAG_ITEM_BLUEPRINT_CAVES_DK,
    PERMFLAG_ITEM_BLUEPRINT_CAVES_DIDDY,
    PERMFLAG_ITEM_BLUEPRINT_CAVES_LANKY,
    PERMFLAG_ITEM_BLUEPRINT_CAVES_TINY,
    PERMFLAG_ITEM_BLUEPRINT_CAVES_CHUNKY,
    PERMFLAG_ITEM_BLUEPRINT_CASTLE_DK,
    PERMFLAG_ITEM_BLUEPRINT_CASTLE_DIDDY,
    PERMFLAG_ITEM_BLUEPRINT_CASTLE_LANKY,
    PERMFLAG_ITEM_BLUEPRINT_CASTLE_TINY,
    PERMFLAG_ITEM_BLUEPRINT_CASTLE_CHUNKY,
    PERMFLAG_ITEM_BLUEPRINT_ISLES_DK,
    PERMFLAG_ITEM_BLUEPRINT_ISLES_DIDDY,
    PERMFLAG_ITEM_BLUEPRINT_ISLES_LANKY,
    PERMFLAG_ITEM_BLUEPRINT_ISLES_TINY,
    PERMFLAG_ITEM_BLUEPRINT_ISLES_CHUNKY,
    // Snide GBs
    PERMFLAG_ITEM_GB_JAPES_BLUEPRINT_DK = 0x1FD,
    PERMFLAG_ITEM_GB_JAPES_BLUEPRINT_DIDDY,
    PERMFLAG_ITEM_GB_JAPES_BLUEPRINT_LANKY,
    PERMFLAG_ITEM_GB_JAPES_BLUEPRINT_TINY,
    PERMFLAG_ITEM_GB_JAPES_BLUEPRINT_CHUNKY,
    PERMFLAG_ITEM_GB_AZTEC_BLUEPRINT_DK,
    PERMFLAG_ITEM_GB_AZTEC_BLUEPRINT_DIDDY,
    PERMFLAG_ITEM_GB_AZTEC_BLUEPRINT_LANKY,
    PERMFLAG_ITEM_GB_AZTEC_BLUEPRINT_TINY,
    PERMFLAG_ITEM_GB_AZTEC_BLUEPRINT_CHUNKY,
    PERMFLAG_ITEM_GB_FACTORY_BLUEPRINT_DK,
    PERMFLAG_ITEM_GB_FACTORY_BLUEPRINT_DIDDY,
    PERMFLAG_ITEM_GB_FACTORY_BLUEPRINT_LANKY,
    PERMFLAG_ITEM_GB_FACTORY_BLUEPRINT_TINY,
    PERMFLAG_ITEM_GB_FACTORY_BLUEPRINT_CHUNKY,
    PERMFLAG_ITEM_GB_GALLEON_BLUEPRINT_DK,
    PERMFLAG_ITEM_GB_GALLEON_BLUEPRINT_DIDDY,
    PERMFLAG_ITEM_GB_GALLEON_BLUEPRINT_LANKY,
    PERMFLAG_ITEM_GB_GALLEON_BLUEPRINT_TINY,
    PERMFLAG_ITEM_GB_GALLEON_BLUEPRINT_CHUNKY,
    PERMFLAG_ITEM_GB_FUNGI_BLUEPRINT_DK,
    PERMFLAG_ITEM_GB_FUNGI_BLUEPRINT_DIDDY,
    PERMFLAG_ITEM_GB_FUNGI_BLUEPRINT_LANKY,
    PERMFLAG_ITEM_GB_FUNGI_BLUEPRINT_TINY,
    PERMFLAG_ITEM_GB_FUNGI_BLUEPRINT_CHUNKY,
    PERMFLAG_ITEM_GB_CAVES_BLUEPRINT_DK,
    PERMFLAG_ITEM_GB_CAVES_BLUEPRINT_DIDDY,
    PERMFLAG_ITEM_GB_CAVES_BLUEPRINT_LANKY,
    PERMFLAG_ITEM_GB_CAVES_BLUEPRINT_TINY,
    PERMFLAG_ITEM_GB_CAVES_BLUEPRINT_CHUNKY,
    PERMFLAG_ITEM_GB_CASTLE_BLUEPRINT_DK,
    PERMFLAG_ITEM_GB_CASTLE_BLUEPRINT_DIDDY,
    PERMFLAG_ITEM_GB_CASTLE_BLUEPRINT_LANKY,
    PERMFLAG_ITEM_GB_CASTLE_BLUEPRINT_TINY,
    PERMFLAG_ITEM_GB_CASTLE_BLUEPRINT_CHUNKY,
    PERMFLAG_ITEM_GB_ISLES_BLUEPRINT_DK,
    PERMFLAG_ITEM_GB_ISLES_BLUEPRINT_DIDDY,
    PERMFLAG_ITEM_GB_ISLES_BLUEPRINT_LANKY,
    PERMFLAG_ITEM_GB_ISLES_BLUEPRINT_TINY,
    PERMFLAG_ITEM_GB_ISLES_BLUEPRINT_CHUNKY,
    // Medals
    PERMFLAG_ITEM_MEDAL_JAPES_DK = 0x225,
    PERMFLAG_ITEM_MEDAL_JAPES_DIDDY,
    PERMFLAG_ITEM_MEDAL_JAPES_LANKY,
    PERMFLAG_ITEM_MEDAL_JAPES_TINY,
    PERMFLAG_ITEM_MEDAL_JAPES_CHUNKY,
    PERMFLAG_ITEM_MEDAL_AZTEC_DK,
    PERMFLAG_ITEM_MEDAL_AZTEC_DIDDY,
    PERMFLAG_ITEM_MEDAL_AZTEC_LANKY,
    PERMFLAG_ITEM_MEDAL_AZTEC_TINY,
    PERMFLAG_ITEM_MEDAL_AZTEC_CHUNKY,
    PERMFLAG_ITEM_MEDAL_FACTORY_DK,
    PERMFLAG_ITEM_MEDAL_FACTORY_DIDDY,
    PERMFLAG_ITEM_MEDAL_FACTORY_LANKY,
    PERMFLAG_ITEM_MEDAL_FACTORY_TINY,
    PERMFLAG_ITEM_MEDAL_FACTORY_CHUNKY,
    PERMFLAG_ITEM_MEDAL_GALLEON_DK,
    PERMFLAG_ITEM_MEDAL_GALLEON_DIDDY,
    PERMFLAG_ITEM_MEDAL_GALLEON_LANKY,
    PERMFLAG_ITEM_MEDAL_GALLEON_TINY,
    PERMFLAG_ITEM_MEDAL_GALLEON_CHUNKY,
    PERMFLAG_ITEM_MEDAL_FUNGI_DK,
    PERMFLAG_ITEM_MEDAL_FUNGI_DIDDY,
    PERMFLAG_ITEM_MEDAL_FUNGI_LANKY,
    PERMFLAG_ITEM_MEDAL_FUNGI_TINY,
    PERMFLAG_ITEM_MEDAL_FUNGI_CHUNKY,
    PERMFLAG_ITEM_MEDAL_CAVES_DK,
    PERMFLAG_ITEM_MEDAL_CAVES_DIDDY,
    PERMFLAG_ITEM_MEDAL_CAVES_LANKY,
    PERMFLAG_ITEM_MEDAL_CAVES_TINY,
    PERMFLAG_ITEM_MEDAL_CAVES_CHUNKY,
    PERMFLAG_ITEM_MEDAL_CASTLE_DK,
    PERMFLAG_ITEM_MEDAL_CASTLE_DIDDY,
    PERMFLAG_ITEM_MEDAL_CASTLE_LANKY,
    PERMFLAG_ITEM_MEDAL_CASTLE_TINY,
    PERMFLAG_ITEM_MEDAL_CASTLE_CHUNKY,
    PERMFLAG_ITEM_MEDAL_HELM_DK,
    PERMFLAG_ITEM_MEDAL_HELM_CHUNKY, // Swapped because of helm
    PERMFLAG_ITEM_MEDAL_HELM_LANKY,
    PERMFLAG_ITEM_MEDAL_HELM_TINY,
    PERMFLAG_ITEM_MEDAL_HELM_DIDDY, // Swapped because of helm
    // Fairies
    PERMFLAG_ITEM_FAIRY_JAPES_POOL = 0x24D,
    PERMFLAG_ITEM_FAIRY_JAPES_PAINTING = 0x24E,
    PERMFLAG_ITEM_FAIRY_FACTORY_FUNKY = 0x24F,
    PERMFLAG_ITEM_FAIRY_GALLEON_CHEST = 0x250,
    PERMFLAG_ITEM_FAIRY_ISLES_FACTORY_LOBBY = 0x251,
    PERMFLAG_ITEM_FAIRY_ISLES_FUNGI_LOBBY = 0x252,
    PERMFLAG_ITEM_FAIRY_FUNGI_ATTIC = 0x253,
    PERMFLAG_ITEM_FAIRY_FUNGI_BOX = 0x254,
    PERMFLAG_ITEM_FAIRY_CAVES_IGLOO = 0x255,
    PERMFLAG_ITEM_FAIRY_HELM_1 = 0x256,
    PERMFLAG_ITEM_FAIRY_HELM_2 = 0x257,
    PERMFLAG_ITEM_FAIRY_AZTEC_LLAMA_TEMPLE = 0x258,
    PERMFLAG_ITEM_FAIRY_AZTEC_5_DOOR_TEMPLE = 0x259,
    PERMFLAG_ITEM_FAIRY_FACTORY_NUMBER_GAME = 0x25A,
    PERMFLAG_ITEM_FAIRY_GALLEON_SHIP = 0x25B,
    PERMFLAG_ITEM_FAIRY_CASTLE_MUSEUM = 0x25C,
    PERMFLAG_ITEM_FAIRY_CASTLE_TREE = 0x25D,
    PERMFLAG_ITEM_FAIRY_ISLES_TREE = 0x25E,
    PERMFLAG_ITEM_FAIRY_ISLES_KREM_ISLES = 0x25F,
    PERMFLAG_ITEM_FAIRY_CAVES_CABIN = 0x260,
    // Crowns
    PERMFLAG_ITEM_CROWN_JAPES = 0x261,
    PERMFLAG_ITEM_CROWN_AZTEC = 0x262,
    PERMFLAG_ITEM_CROWN_FACTORY = 0x263,
    PERMFLAG_ITEM_CROWN_GALLEON = 0x264,
    PERMFLAG_ITEM_CROWN_FUNGI = 0x265,
    PERMFLAG_ITEM_CROWN_ISLES_LOBBY = 0x266,
    PERMFLAG_ITEM_CROWN_ISLES_SNIDE = 0x267,
    PERMFLAG_ITEM_CROWN_CAVES = 0x268,
    PERMFLAG_ITEM_CROWN_CASTLE = 0x269,
    PERMFLAG_ITEM_CROWN_HELM = 0x26A,
    //
    PERMFLAG_PROGRESS_HELM_SHUTDOWN = 0x302,
    PERMFLAG_PROGRESS_CROWN_DOOR_OPENED = 0x304,
    // Captions
    PERMFLAG_CAPTION_FUNKY = 0x307,
    PERMFLAG_CAPTION_SNIDE = 0x308,
    PERMFLAG_CAPTION_CRANKY = 0x309,
    PERMFLAG_CAPTION_CANDY = 0x30A,
    PERMFLAG_CAPTION_JAPES = 0x30B,
    PERMFLAG_CAPTION_FACTORY = 0x30C,
    PERMFLAG_CAPTION_GALLEON = 0x30D,
    PERMFLAG_CAPTION_FUNGI = 0x30E,
    PERMFLAG_CAPTION_CAVES = 0x30F,
    PERMFLAG_CAPTION_CASTLE = 0x310,
    PERMFLAG_CAPTION_TROFFNSCOFF = 0x311,
    PERMFLAG_CAPTION_HELM = 0x312,
    PERMFLAG_CAPTION_AZTEC = 0x313,
    //
    PERMFLAG_PROGRESS_HAS_QUIT_GAME = 0x314,
    PERMFLAG_PROGRESS_K_LUMSY_FREE = 0x315
} permflag_e;

typedef enum tempflag_e {
    TEMPFLAG_TEXT_DIDDY_CAGE = 0x00,
    TEMPFLAG_TEXT_LANKY_CAGE = 0x06,
    TEMPFLAG_TEXT_LLAMA_CAGE = 0x07,
    TEMPFLAG_TEXT_TINY_CAGE = 0x08,
    TEMPFLAG_TEXT_CHUNKY_CAGE = 0x0F,
    TEMPFLAG_ARCADE_GB_SPAWN_PENDING = 0x10,
    TEMPFLAG_NINTENDO_COIN_SPAWN_PENDING = 0x11,
    TEMPFLAG_FACTORY_CAR_RACE_INTRO = 0x12,
    TEMPFLAG_CUTSCENE_MERMAID_FTCS = 0x1E,
    TEMPFLAG_FTT_NO_BEAN = 0x21,
    TEMPFLAG_ICE_TOMATO_BOARD_ACTIVE = 0x30,
    TEMPFLAG_CASTLE_CAR_INTRO = 0x38,
    TEMPFLAG_K_ROOL_TOE_1_DAMAGED = 0x51,
    TEMPFLAG_K_ROOL_TOE_2_DAMAGED = 0x52,
    TEMPFLAG_K_ROOL_TOE_3_DAMAGED = 0x53,
    TEMPFLAG_K_ROOL_TOE_4_DAMAGED = 0x54,
    TEMPFLAG_UNK_55 = 0x55,
    TEMPFLAG_K_ROOL_TIMEOUT = 0x5A,
    TEMPFLAG_K_ROOL_CRANKY_CS_PLAYED = 0x5B,
    TEMPFLAG_RESET_TINY_PHASE = 0x5C,
    TEMPFLAG_K_ROOL_DK_INTRO = 0x5D,
    TEMPFLAG_K_ROOL_GONE_CUTSCENE = 0x5F,
    TEMPFLAG_JETPAC_IN_STORY_MODE = 0x61,
    TEMPFLAG_RAREWARE_COIN_SPAWN_PENDING = 0x62,
    TEMPFLAG_ARCADE_IN_STORY_MODE = 0x63,
    TEMPFLAG_TRAINING_SPAWN_PENDING = 0x64,
    TEMPFLAG_CAVES_BEETLE_INTRO = 0x65,
    TEMPFLAG_AZTEC_BEETLE_INTRO = 0x66,
    TEMPFLAG_AZTEC_DOGADON_INTRO = 0x67,
    TEMPFLAG_JAPES_DILLO_INTRO = 0x68,
    TEMPFLAG_FUNGI_DOGADON_INTRO = 0x69,
    TEMPFLAG_FACTORY_JACK_INTRO = 0x6A,
    TEMPFLAG_GALLEON_PUFFTOSS_INTRO = 0x6B,
    TEMPFLAG_CASTLE_KUTOUT_INTRO = 0x6C,
    TEMPFLAG_CAVES_DILLO_INTRO = 0x6D,
    TEMPFLAG_PLAYER_STUCK_CUTSCENE = 0x6E
} tempflag_e;

typedef enum globalflag_e {
    GLOBALFLAG_MYSTERY_MENU = 0x0,
    GLOBALFLAG_CUTSCENE_MENU = 0x1,
    GLOBALFLAG_CUTSCENE_JAPES_INTRO = 0x2,
    GLOBALFLAG_CUTSCENE_AZTEC_INTRO = 0x3,
    GLOBALFLAG_CUTSCENE_FACTORY_INTRO = 0x4,
    GLOBALFLAG_CUTSCENE_GALLEON_INTRO = 0x5,
    GLOBALFLAG_CUTSCENE_FUNGI_INTRO = 0x6,
    GLOBALFLAG_CUTSCENE_CAVES_INTRO = 0x7,
    GLOBALFLAG_CUTSCENE_CASTLE_INTRO = 0x8,
    GLOBALFLAG_CUTSCENE_ENTER_HIDEOUT = 0x9,
    GLOBALFLAG_CUTSCENE_PRESS_BUTTON = 0xA,
    GLOBALFLAG_CUTSCENE_KROOL_TAKEOFF = 0xB,
    GLOBALFLAG_CUTSCENE_GAME_OVER = 0xC,
    GLOBALFLAG_CUTSCENE_END_SEQUENCE = 0xD,
    GLOBALFLAG_MINIGAME_MENU = 0xE,
    GLOBALFLAG_MINIGAME_RAMBI_ARENA = 0xF,
    GLOBALFLAG_MINIGAME_ENGUARDE_ARENA = 0x10,
    GLOBALFLAG_MINIGAME_DK_ARCADE = 0x11,
    GLOBALFLAG_MINIGAME_JETPAC = 0x12,
    GLOBALFLAG_BOSSES_MENU = 0x13,
    GLOBALFLAG_BOSSES_JAPES = 0x14,
    GLOBALFLAG_BOSSES_AZTEC = 0x15,
    GLOBALFLAG_BOSSES_FACTORY = 0x16,
    GLOBALFLAG_BOSSES_GALLEON = 0x17,
    GLOBALFLAG_BOSSES_FUNGI = 0x18,
    GLOBALFLAG_BOSSES_CAVES = 0x19,
    GLOBALFLAG_BOSSES_CASTLE = 0x1A,
    GLOBALFLAG_BOSSES_THE_MAIN_EVENT = 0x1B,
    GLOBALFLAG_MULTIPLAYER_MENU = 0x1C,
    GLOBALFLAG_MULTIPLAYER_DIDDY = 0x1D,
    GLOBALFLAG_MULTIPLAYER_LANKY = 0x1E,
    GLOBALFLAG_MULTIPLAYER_TINY = 0x1F,
    GLOBALFLAG_MULTIPLAYER_CHUNKY = 0x20,
    GLOBALFLAG_KRUSHA_MENU = 0x21,
    GLOBALFLAG_CHEATS_MENU = 0x22,
    GLOBALFLAG_UNK_23 = 0x23,
    GLOBALFLAG_UNK_24 = 0x24,
    GLOBALFLAG_DEBUG_KLUMSY_1 = 0x26,
    GLOBALFLAG_DEBUG_KLUMSY_2 = 0x28,
    GLOBALFLAG_ENDING_KLUMSY = 0x2A,
    GLOBALFLAG_ENDING_BLOOPERS = 0x2B
} globalflag_e;

typedef union flag_e {
    permflag_e;
    tempflag_e;
    globalflag_e;
} flag_e;

typedef enum pointertable_e {
    TABLE_00_MIDI,
    TABLE_01_MAP_GEOMETRY,
    TABLE_02_MAP_WALLS,
    TABLE_03_MAP_FLOORS,
    TABLE_04_PROP_GEOMETRY,
    TABLE_05_ACTOR_GEOMETRY,
    TABLE_06_UNUSED,
    TABLE_07_TEXTURES_UNCOMPRESSED,
    TABLE_08_CUTSCENES,
    TABLE_09_SETUP,
    TABLE_10_SCRIPTS,
    TABLE_11_ANIMATIONS,
    TABLE_12_TEXT,
    TABLE_13_ANIM_CODE,
    TABLE_14_TEXTURES_HUD,
    TABLE_15_PATHS,
    TABLE_16_SPAWNERS,
    TABLE_17_DKTV,
    TABLE_18_TRIGGERS,
    TABLE_19_UNKNOWN,
    TABLE_20_UNKNOWN,
    TABLE_21_AUTOWALKS,
    TABLE_22_CRITTERS,
    TABLE_23_EXITS,
    TABLE_24_CHECKPOINTS,
    TABLE_25_TEXTURES_GEOMETRY,
    TABLE_26_UNCOMPRESSED_SIZES,
    TABLE_27_UNUSED,
    TABLE_28_UNUSED,
    TABLE_29_UNUSED,
    TABLE_30_UNUSED,
    TABLE_31_UNUSED
} pointertable_e;

#endif
