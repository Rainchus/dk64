#ifndef _TYPES_H_
#define _TYPES_H_

#include <ultra64.h>

/// Linker symbol address, as in `ld_addrs.h`.
typedef u8 Addr[];

#endif
