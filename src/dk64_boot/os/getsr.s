.include "macro.inc"

.set noat
.set noreorder
.set gp=64

.set fp=64

.section .text, "ax"

/* Generated by spimdisasm 1.32.1 */

/* Handwritten function */
glabel __osGetSR
    /* AF80 8000A380 40026000 */  mfc0       $v0, $12 /* handwritten instruction */
    /* AF84 8000A384 03E00008 */  jr         $ra
    /* AF88 8000A388 00000000 */   nop
    /* AF8C 8000A38C 00000000 */  nop
.size __osGetSR, . - __osGetSR
