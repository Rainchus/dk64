#include "common.h"


void __osDequeueThread(OSThread **queue, OSThread *t)
{
	register OSThread *pred;
	register OSThread *succ;
	pred = (OSThread *)queue; // This is actually legit..
	succ = pred->next;

	while (succ != NULL) {
		if (succ == t) {
			pred->next = t->next;
			return;
		}

		pred = succ;
		succ = pred->next;
	}
}
