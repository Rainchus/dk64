#include "common.h"

void func_global_asm_807382A0(ALSeqPlayer *seqp, s32 ticks, u8 status, u8 byte1, u8 byte2) {
    ALEvent evt;
    ALMicroTime deltaTIme;

    evt.type = AL_SEQP_MIDI_EVT;
    evt.msg.midi.ticks = 0;
    evt.msg.midi.status = status;
    evt.msg.midi.byte1 = byte1;
    evt.msg.midi.byte2 = byte2;
    evt.msg.midi.duration = 0;
    deltaTIme = ticks;
    alEvtqPostEvent(&seqp->evtq, &evt, deltaTIme);
}
