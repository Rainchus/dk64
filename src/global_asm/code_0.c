#include "common.h"

#pragma GLOBAL_ASM("asm/nonmatchings/global_asm/code_0/func_global_asm_805FB300.s")

void func_global_asm_805FB5C4(OSMesgQueue *, s32);
void func_global_asm_805FBFF4(void *);
extern void func_dk64_boot_80000450(s32 devAddr, s32 arg1, void *dramAddr);
extern OverlayInfoStruct gOverlayTable[];
extern void func_arcade_80024000(void);
extern void func_boss_80024000(void);
extern void func_menu_80024000(MenuStruct80024000 *);
extern void func_jetpac_80024000(void);
extern Gfx *func_bonus_80024000(Gfx *, Actor *);
extern void func_race_80024000(void);
extern void func_critter_80024000(Critter *, u8 *, f32);
extern void func_minecart_80024000(u8, u8);
extern OSMesgQueue D_80761050;
extern void *D_80761068;
extern OSThread D_global_asm_80761430;
extern u64 gStackCanary;

/* .bss */
OSMesgQueue *D_global_asm_807655E0;

// Solely for pointers
// extern s32 D_80010720;
// extern s32 D_80016630;
// extern s32 D_global_asm_807FF100;
// extern s32 D_80027110;
// extern s32 D_80027100;
// extern s32 D_80028E20;
// extern s32 D_80028E10;
// extern s32 D_8002DF10;
// extern s8 D_bonus_8002DEF0[];
// extern s32 D_80030170;
// extern s32 D_80030160;
// extern s32 D_8002A1E0;
// extern s32 D_critter_8002A1B0;
// extern s32 D_80036DF0;
// extern s32 D_80036DC0;
// extern s32 D_80045C00;
// extern s32 D_jetpac_8002EC30;
// extern s32 D_80033FD0;
// extern s32 D_80033F10;
// extern s32 D_8004C750;
// extern s32 D_arcade_8004AC00;

#define setOverlay(i, start, ovl_end, code_end) \
    gOverlayTable[i].rdram_start = start; \
    gOverlayTable[i].overlay_end = ovl_end; \
    gOverlayTable[i].rdram_code_end = code_end; \
    gOverlayTable[i].rdram_data_end = ovl_end;

/*
void func_global_asm_805FB300(OSMesgQueue* arg0, s32 arg1) {
    if (arg1 != 0) {
        func_global_asm_805FB5C4();
    }
    D_global_asm_807655E0 = arg0;
    SetOverlay(0, dk64_boot_VRAM, dk64_boot_bss_VRAM, dk64_boot_VRAM_END);
    SetOverlay(1, global_asm_VRAM, global_asm_bss_VRAM, global_asm_VRAM_END);
    SetOverlay(2, multiplayer_VRAM, multiplayer_bss_VRAM, multiplayer_VRAM_END);
    SetOverlay(3, minecart_VRAM, minecart_bss_VRAM, minecart_VRAM_END);
    SetOverlay(4, bonus_VRAM, bonus_bss_VRAM, bonus_VRAM_END);
    SetOverlay(5, race_VRAM, race_bss_VRAM, race_VRAM_END);
    SetOverlay(6, critter_VRAM, critter_bss_VRAM, critter_VRAM_END);
    SetOverlay(7, boss_VRAM, boss_bss_VRAM, boss_VRAM_END);
    SetOverlay(8, jetpac_VRAM, jetpac_bss_VRAM, jetpac_VRAM_END);
    SetOverlay(8, jetpac_VRAM, jetpac_bss_VRAM, jetpac_VRAM_END);
    SetOverlay(9, menu_VRAM, menu_bss_VRAM, menu_VRAM_END);
    SetOverlay(10, arcade_VRAM, arcade_bss_VRAM, arcade_VRAM_END);
    osDpSetStatus(4U);
    osCreatePiManager(0x96, &D_global_asm_80761050, &D_global_asm_80761068, 0xF2);
    gStackCanary = 0x12345678;
    osCreateThread(&D_global_asm_80761430, 3, func_global_asm_805FBFF4, NULL, &D_global_asm_807655E0, 0xA);
    osStartThread(&D_global_asm_80761430);
}
*/

typedef struct {
    s32 unk0;
    s32 unk4;
    s32 unk8;
} Struct80744464;

void __osSpSetStatus(u32);
extern OSThread D_global_asm_80761430;
extern s8 D_global_asm_80744460;
extern Struct80744464 D_global_asm_80744464;
#define BUFFER_TIME OS_NSEC_TO_CYCLES(48484843)


void func_global_asm_805FB5C4(OSMesgQueue *arg0, s32 arg1) {
    OSTime target_time;
    Struct80744464 sp34;
    OSTime buffer_time;
    u8 buffer[1];
    void *sp20;
    u8 buffer2[5];
    static OSTime D_global_asm_807655E8;

    sp34 = D_global_asm_80744464;
    if (arg1 == 2) {
        osViBlack(1U);
        func_global_asm_80601CF0(1);
        D_global_asm_80744460 = 1;
        while (TRUE) {}
    }
    osRecvMesg(arg0, &sp20, 1);
    D_global_asm_80744460 = 1;
    func_global_asm_80601CF0(1);
    osStopThread(&D_global_asm_80761430);
    osSetThreadPri(NULL, 0xB);
    D_global_asm_807655E8 = osGetTime();
    while (osGetTime() < D_global_asm_807655E8 + BUFFER_TIME);
    osViBlack(1);
    __osSpSetStatus(0xAAAA82);
    osDpSetStatus(0x1D6);
    func_global_asm_8060E930();
    while (TRUE) {}
}
