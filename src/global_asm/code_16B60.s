.include "macro.inc"

.set noat
.set noreorder
.set gp=64

.set fp=64

.section .text, "ax"

/* Generated by spimdisasm 1.32.1 */

glabel func_global_asm_80611E60
    /* 2016B60 80611E60 27BDFFF0 */  addiu      $sp, $sp, -0x10
    /* 2016B64 80611E64 FFBF0000 */  sd         $ra, 0x0($sp)
    /* 2016B68 80611E68 FFBE0008 */  sd         $fp, 0x8($sp)
    /* 2016B6C 80611E6C 0C1847A2 */  jal        func_global_asm_80611E88
    /* 2016B70 80611E70 00801825 */   or        $v1, $a0, $zero
    /* 2016B74 80611E74 DFBF0000 */  ld         $ra, 0x0($sp)
    /* 2016B78 80611E78 03C01025 */  or         $v0, $fp, $zero
    /* 2016B7C 80611E7C DFBE0008 */  ld         $fp, 0x8($sp)
    /* 2016B80 80611E80 03E00008 */  jr         $ra
    /* 2016B84 80611E84 27BD0010 */   addiu     $sp, $sp, 0x10
.size func_global_asm_80611E60, . - func_global_asm_80611E60

/* Handwritten function */
glabel func_global_asm_80611E88
    /* 2016B88 80611E88 27BDFFE0 */  addiu      $sp, $sp, -0x20
    /* 2016B8C 80611E8C FFA50018 */  sd         $a1, 0x18($sp)
    /* 2016B90 80611E90 00032982 */  srl        $a1, $v1, 6
    /* 2016B94 80611E94 28A103FF */  slti       $at, $a1, 0x3FF
    /* 2016B98 80611E98 FFBF0000 */  sd         $ra, 0x0($sp)
    /* 2016B9C 80611E9C FFA30008 */  sd         $v1, 0x8($sp)
    /* 2016BA0 80611EA0 14200008 */  bnez       $at, .Lglobal_asm_80611EC4
    /* 2016BA4 80611EA4 FFA40010 */   sd        $a0, 0x10($sp)
    /* 2016BA8 80611EA8 3063003F */  andi       $v1, $v1, 0x3F
    /* 2016BAC 80611EAC 3C048061 */  lui        $a0, %hi(func_global_asm_80612710)
    /* 2016BB0 80611EB0 00031840 */  sll        $v1, $v1, 1
    /* 2016BB4 80611EB4 24842710 */  addiu      $a0, $a0, %lo(func_global_asm_80612710)
    /* 2016BB8 80611EB8 00832020 */  add        $a0, $a0, $v1 /* handwritten instruction */
    /* 2016BBC 80611EBC 1000000D */  b          .Lglobal_asm_80611EF4
    /* 2016BC0 80611EC0 949E0000 */   lhu       $fp, 0x0($a0)
  .Lglobal_asm_80611EC4:
    /* 2016BC4 80611EC4 3C048061 */  lui        $a0, %hi(D_global_asm_80611F0C)
    /* 2016BC8 80611EC8 24841F0C */  addiu      $a0, $a0, %lo(D_global_asm_80611F0C)
    /* 2016BCC 80611ECC 00052840 */  sll        $a1, $a1, 1
    /* 2016BD0 80611ED0 00A42820 */  add        $a1, $a1, $a0 /* handwritten instruction */
    /* 2016BD4 80611ED4 94BE0000 */  lhu        $fp, 0x0($a1)
    /* 2016BD8 80611ED8 94A50002 */  lhu        $a1, 0x2($a1)
    /* 2016BDC 80611EDC 3063003F */  andi       $v1, $v1, 0x3F
    /* 2016BE0 80611EE0 00BE2022 */  sub        $a0, $a1, $fp /* handwritten instruction */
    /* 2016BE4 80611EE4 00830018 */  mult       $a0, $v1
    /* 2016BE8 80611EE8 00002012 */  mflo       $a0
    /* 2016BEC 80611EEC 00042182 */  srl        $a0, $a0, 6
    /* 2016BF0 80611EF0 03C4F020 */  add        $fp, $fp, $a0 /* handwritten instruction */
  .Lglobal_asm_80611EF4:
    /* 2016BF4 80611EF4 DFBF0000 */  ld         $ra, 0x0($sp)
    /* 2016BF8 80611EF8 DFA30008 */  ld         $v1, 0x8($sp)
    /* 2016BFC 80611EFC DFA40010 */  ld         $a0, 0x10($sp)
    /* 2016C00 80611F00 DFA50018 */  ld         $a1, 0x18($sp)
    /* 2016C04 80611F04 03E00008 */  jr         $ra
    /* 2016C08 80611F08 27BD0020 */   addiu     $sp, $sp, 0x20
.size func_global_asm_80611E88, . - func_global_asm_80611E88

glabel D_global_asm_80611F0C
    /* 2016C0C 80611F0C 0000000A */ .word 0x0000000A
    /* 2016C10 80611F10 0014001E */ .word 0x0014001E
    /* 2016C14 80611F14 00280032 */ .word 0x00280032
    /* 2016C18 80611F18 003D0047 */ .word 0x003D0047 /* invalid instruction */
    /* 2016C1C 80611F1C 0051005B */ .word 0x0051005B /* invalid instruction */
    /* 2016C20 80611F20 00650070 */ .word 0x00650070
    /* 2016C24 80611F24 007A0084 */ .word 0x007A0084 /* invalid instruction */
    /* 2016C28 80611F28 008E0098 */ .word 0x008E0098 /* invalid instruction */
    /* 2016C2C 80611F2C 00A200AD */ .word 0x00A200AD /* invalid instruction */
    /* 2016C30 80611F30 00B700C1 */ .word 0x00B700C1 /* invalid instruction */
    /* 2016C34 80611F34 00CB00D5 */ .word 0x00CB00D5 /* invalid instruction */
    /* 2016C38 80611F38 00E000EA */ .word 0x00E000EA /* invalid instruction */
    /* 2016C3C 80611F3C 00F400FE */ .word 0x00F400FE /* invalid instruction */
    /* 2016C40 80611F40 01080113 */ .word 0x01080113 /* invalid instruction */
    /* 2016C44 80611F44 011D0127 */ .word 0x011D0127 /* invalid instruction */
    /* 2016C48 80611F48 0131013B */ .word 0x0131013B /* invalid instruction */
    /* 2016C4C 80611F4C 01460150 */ .word 0x01460150 /* invalid instruction */
    /* 2016C50 80611F50 015A0164 */ .word 0x015A0164 /* invalid instruction */
    /* 2016C54 80611F54 016E0178 */ .word 0x016E0178 /* invalid instruction */
    /* 2016C58 80611F58 0183018D */ .word 0x0183018D
    /* 2016C5C 80611F5C 019701A1 */ .word 0x019701A1 /* invalid instruction */
    /* 2016C60 80611F60 01AB01B6 */ .word 0x01AB01B6
    /* 2016C64 80611F64 01C001CA */ .word 0x01C001CA /* invalid instruction */
    /* 2016C68 80611F68 01D401DE */ .word 0x01D401DE /* invalid instruction */
    /* 2016C6C 80611F6C 01E901F3 */ .word 0x01E901F3
    /* 2016C70 80611F70 01FD0207 */ .word 0x01FD0207 /* invalid instruction */
    /* 2016C74 80611F74 0211021C */ .word 0x0211021C /* invalid instruction */
    /* 2016C78 80611F78 02260230 */ .word 0x02260230
    /* 2016C7C 80611F7C 023A0244 */ .word 0x023A0244 /* invalid instruction */
    /* 2016C80 80611F80 024F0259 */ .word 0x024F0259 /* invalid instruction */
    /* 2016C84 80611F84 0263026D */ .word 0x0263026D /* invalid instruction */
    /* 2016C88 80611F88 02770282 */ .word 0x02770282 /* invalid instruction */
    /* 2016C8C 80611F8C 028C0296 */ .word 0x028C0296 /* invalid instruction */
    /* 2016C90 80611F90 02A002AA */ .word 0x02A002AA /* invalid instruction */
    /* 2016C94 80611F94 02B502BF */ .word 0x02B502BF /* invalid instruction */
    /* 2016C98 80611F98 02C902D3 */ .word 0x02C902D3 /* invalid instruction */
    /* 2016C9C 80611F9C 02DD02E8 */ .word 0x02DD02E8 /* invalid instruction */
    /* 2016CA0 80611FA0 02F202FC */ .word 0x02F202FC /* invalid instruction */
    /* 2016CA4 80611FA4 03060311 */ .word 0x03060311 /* invalid instruction */
    /* 2016CA8 80611FA8 031B0325 */ .word 0x031B0325 /* invalid instruction */
    /* 2016CAC 80611FAC 032F0339 */ .word 0x032F0339 /* invalid instruction */
    /* 2016CB0 80611FB0 0344034E */ .word 0x0344034E /* invalid instruction */
    /* 2016CB4 80611FB4 03580362 */ .word 0x03580362 /* invalid instruction */
    /* 2016CB8 80611FB8 036D0377 */ .word 0x036D0377 /* invalid instruction */
    /* 2016CBC 80611FBC 0381038B */ .word 0x0381038B /* invalid instruction */
    /* 2016CC0 80611FC0 039503A0 */ .word 0x039503A0 /* invalid instruction */
    /* 2016CC4 80611FC4 03AA03B4 */ .word 0x03AA03B4
    /* 2016CC8 80611FC8 03BE03C9 */ .word 0x03BE03C9 /* invalid instruction */
    /* 2016CCC 80611FCC 03D303DD */ .word 0x03D303DD /* invalid instruction */
    /* 2016CD0 80611FD0 03E703F1 */ .word 0x03E703F1
    /* 2016CD4 80611FD4 03FC0406 */ .word 0x03FC0406 /* invalid instruction */
    /* 2016CD8 80611FD8 0410041A */ .word 0x0410041A
    /* 2016CDC 80611FDC 0425042F */ .word 0x0425042F /* invalid instruction */
    /* 2016CE0 80611FE0 04390443 */ .word 0x04390443 /* invalid instruction */
    /* 2016CE4 80611FE4 044E0458 */ .word 0x044E0458
    /* 2016CE8 80611FE8 0462046C */ .word 0x0462046C
    /* 2016CEC 80611FEC 04770481 */ .word 0x04770481 /* invalid instruction */
    /* 2016CF0 80611FF0 048B0495 */ .word 0x048B0495
    /* 2016CF4 80611FF4 04A004AA */ .word 0x04A004AA
    /* 2016CF8 80611FF8 04B404BE */ .word 0x04B404BE /* invalid instruction */
    /* 2016CFC 80611FFC 04C904D3 */ .word 0x04C904D3
    /* 2016D00 80612000 04DD04E7 */ .word 0x04DD04E7 /* invalid instruction */
    /* 2016D04 80612004 04F204FC */ .word 0x04F204FC
    /* 2016D08 80612008 05060510 */ .word 0x05060510 /* invalid instruction */
    /* 2016D0C 8061200C 051B0525 */ .word 0x051B0525 /* invalid instruction */
    /* 2016D10 80612010 052F053A */ .word 0x052F053A /* invalid instruction */
    /* 2016D14 80612014 0544054E */ .word 0x0544054E /* invalid instruction */
    /* 2016D18 80612018 05580563 */ .word 0x05580563 /* invalid instruction */
    /* 2016D1C 8061201C 056D0577 */ .word 0x056D0577 /* invalid instruction */
    /* 2016D20 80612020 0581058C */ .word 0x0581058C
    /* 2016D24 80612024 059605A0 */ .word 0x059605A0 /* invalid instruction */
    /* 2016D28 80612028 05AB05B5 */ .word 0x05AB05B5
    /* 2016D2C 8061202C 05BF05C9 */ .word 0x05BF05C9 /* invalid instruction */
    /* 2016D30 80612030 05D405DE */ .word 0x05D405DE /* invalid instruction */
    /* 2016D34 80612034 05E805F3 */ .word 0x05E805F3
    /* 2016D38 80612038 05FD0607 */ .word 0x05FD0607 /* invalid instruction */
    /* 2016D3C 8061203C 0612061C */ .word 0x0612061C
    /* 2016D40 80612040 06260630 */ .word 0x06260630 /* invalid instruction */
    /* 2016D44 80612044 063B0645 */ .word 0x063B0645 /* invalid instruction */
    /* 2016D48 80612048 064F065A */ .word 0x064F065A /* invalid instruction */
    /* 2016D4C 8061204C 0664066E */ .word 0x0664066E /* invalid instruction */
    /* 2016D50 80612050 06790683 */ .word 0x06790683 /* invalid instruction */
    /* 2016D54 80612054 068D0698 */ .word 0x068D0698 /* invalid instruction */
    /* 2016D58 80612058 06A206AC */ .word 0x06A206AC
    /* 2016D5C 8061205C 06B706C1 */ .word 0x06B706C1 /* invalid instruction */
    /* 2016D60 80612060 06CB06D5 */ .word 0x06CB06D5
    /* 2016D64 80612064 06E006EA */ .word 0x06E006EA
    /* 2016D68 80612068 06F406FF */ .word 0x06F406FF /* invalid instruction */
    /* 2016D6C 8061206C 07090714 */ .word 0x07090714
    /* 2016D70 80612070 071E0728 */ .word 0x071E0728 /* invalid instruction */
    /* 2016D74 80612074 0733073D */ .word 0x0733073D
    /* 2016D78 80612078 07470752 */ .word 0x07470752 /* invalid instruction */
    /* 2016D7C 8061207C 075C0766 */ .word 0x075C0766 /* invalid instruction */
    /* 2016D80 80612080 0771077B */ .word 0x0771077B
    /* 2016D84 80612084 07850790 */ .word 0x07850790 /* invalid instruction */
    /* 2016D88 80612088 079A07A4 */ .word 0x079A07A4 /* invalid instruction */
    /* 2016D8C 8061208C 07AF07B9 */ .word 0x07AF07B9 /* invalid instruction */
    /* 2016D90 80612090 07C407CE */ .word 0x07C407CE /* invalid instruction */
    /* 2016D94 80612094 07D807E3 */ .word 0x07D807E3 /* invalid instruction */
    /* 2016D98 80612098 07ED07F7 */ .word 0x07ED07F7 /* invalid instruction */
    /* 2016D9C 8061209C 0802080C */ .word 0x0802080C
    /* 2016DA0 806120A0 08170821 */ .word 0x08170821
    /* 2016DA4 806120A4 082B0836 */ .word 0x082B0836
    /* 2016DA8 806120A8 0840084B */ .word 0x0840084B
    /* 2016DAC 806120AC 0855085F */ .word 0x0855085F
    /* 2016DB0 806120B0 086A0874 */ .word 0x086A0874
    /* 2016DB4 806120B4 087F0889 */ .word 0x087F0889
    /* 2016DB8 806120B8 0893089E */ .word 0x0893089E
    /* 2016DBC 806120BC 08A808B3 */ .word 0x08A808B3
    /* 2016DC0 806120C0 08BD08C8 */ .word 0x08BD08C8
    /* 2016DC4 806120C4 08D208DC */ .word 0x08D208DC
    /* 2016DC8 806120C8 08E708F1 */ .word 0x08E708F1
    /* 2016DCC 806120CC 08FC0906 */ .word 0x08FC0906
    /* 2016DD0 806120D0 0911091B */ .word 0x0911091B
    /* 2016DD4 806120D4 09260930 */ .word 0x09260930
    /* 2016DD8 806120D8 093A0945 */ .word 0x093A0945
    /* 2016DDC 806120DC 094F095A */ .word 0x094F095A
    /* 2016DE0 806120E0 0964096F */ .word 0x0964096F
    /* 2016DE4 806120E4 09790984 */ .word 0x09790984
    /* 2016DE8 806120E8 098E0999 */ .word 0x098E0999
    /* 2016DEC 806120EC 09A309AE */ .word 0x09A309AE
    /* 2016DF0 806120F0 09B809C3 */ .word 0x09B809C3
    /* 2016DF4 806120F4 09CD09D7 */ .word 0x09CD09D7
    /* 2016DF8 806120F8 09E209EC */ .word 0x09E209EC
    /* 2016DFC 806120FC 09F70A01 */ .word 0x09F70A01
    /* 2016E00 80612100 0A0C0A16 */ .word 0x0A0C0A16
    /* 2016E04 80612104 0A210A2C */ .word 0x0A210A2C
    /* 2016E08 80612108 0A360A41 */ .word 0x0A360A41
    /* 2016E0C 8061210C 0A4B0A56 */ .word 0x0A4B0A56
    /* 2016E10 80612110 0A600A6B */ .word 0x0A600A6B
    /* 2016E14 80612114 0A750A80 */ .word 0x0A750A80
    /* 2016E18 80612118 0A8A0A95 */ .word 0x0A8A0A95
    /* 2016E1C 8061211C 0A9F0AAA */ .word 0x0A9F0AAA
    /* 2016E20 80612120 0AB40ABF */ .word 0x0AB40ABF
    /* 2016E24 80612124 0AC90AD4 */ .word 0x0AC90AD4
    /* 2016E28 80612128 0ADF0AE9 */ .word 0x0ADF0AE9
    /* 2016E2C 8061212C 0AF40AFE */ .word 0x0AF40AFE
    /* 2016E30 80612130 0B090B13 */ .word 0x0B090B13
    /* 2016E34 80612134 0B1E0B29 */ .word 0x0B1E0B29
    /* 2016E38 80612138 0B330B3E */ .word 0x0B330B3E
    /* 2016E3C 8061213C 0B480B53 */ .word 0x0B480B53
    /* 2016E40 80612140 0B5E0B68 */ .word 0x0B5E0B68
    /* 2016E44 80612144 0B730B7D */ .word 0x0B730B7D
    /* 2016E48 80612148 0B880B93 */ .word 0x0B880B93
    /* 2016E4C 8061214C 0B9D0BA8 */ .word 0x0B9D0BA8
    /* 2016E50 80612150 0BB20BBD */ .word 0x0BB20BBD
    /* 2016E54 80612154 0BC80BD2 */ .word 0x0BC80BD2
    /* 2016E58 80612158 0BDD0BE8 */ .word 0x0BDD0BE8
    /* 2016E5C 8061215C 0BF20BFD */ .word 0x0BF20BFD
    /* 2016E60 80612160 0C070C12 */ .word 0x0C070C12
    /* 2016E64 80612164 0C1D0C27 */ .word 0x0C1D0C27
    /* 2016E68 80612168 0C320C3D */ .word 0x0C320C3D
    /* 2016E6C 8061216C 0C470C52 */ .word 0x0C470C52
    /* 2016E70 80612170 0C5D0C67 */ .word 0x0C5D0C67
    /* 2016E74 80612174 0C720C7D */ .word 0x0C720C7D
    /* 2016E78 80612178 0C870C92 */ .word 0x0C870C92
    /* 2016E7C 8061217C 0C9D0CA8 */ .word 0x0C9D0CA8
    /* 2016E80 80612180 0CB20CBD */ .word 0x0CB20CBD
    /* 2016E84 80612184 0CC80CD2 */ .word 0x0CC80CD2
    /* 2016E88 80612188 0CDD0CE8 */ .word 0x0CDD0CE8
    /* 2016E8C 8061218C 0CF30CFD */ .word 0x0CF30CFD
    /* 2016E90 80612190 0D080D13 */ .word 0x0D080D13
    /* 2016E94 80612194 0D1D0D28 */ .word 0x0D1D0D28
    /* 2016E98 80612198 0D330D3E */ .word 0x0D330D3E
    /* 2016E9C 8061219C 0D480D53 */ .word 0x0D480D53
    /* 2016EA0 806121A0 0D5E0D69 */ .word 0x0D5E0D69
    /* 2016EA4 806121A4 0D730D7E */ .word 0x0D730D7E
    /* 2016EA8 806121A8 0D890D94 */ .word 0x0D890D94
    /* 2016EAC 806121AC 0D9F0DA9 */ .word 0x0D9F0DA9
    /* 2016EB0 806121B0 0DB40DBF */ .word 0x0DB40DBF
    /* 2016EB4 806121B4 0DCA0DD5 */ .word 0x0DCA0DD5
    /* 2016EB8 806121B8 0DDF0DEA */ .word 0x0DDF0DEA
    /* 2016EBC 806121BC 0DF50E00 */ .word 0x0DF50E00
    /* 2016EC0 806121C0 0E0B0E15 */ .word 0x0E0B0E15
    /* 2016EC4 806121C4 0E200E2B */ .word 0x0E200E2B
    /* 2016EC8 806121C8 0E360E41 */ .word 0x0E360E41
    /* 2016ECC 806121CC 0E4C0E56 */ .word 0x0E4C0E56
    /* 2016ED0 806121D0 0E610E6C */ .word 0x0E610E6C
    /* 2016ED4 806121D4 0E770E82 */ .word 0x0E770E82
    /* 2016ED8 806121D8 0E8D0E98 */ .word 0x0E8D0E98
    /* 2016EDC 806121DC 0EA30EAD */ .word 0x0EA30EAD
    /* 2016EE0 806121E0 0EB80EC3 */ .word 0x0EB80EC3
    /* 2016EE4 806121E4 0ECE0ED9 */ .word 0x0ECE0ED9
    /* 2016EE8 806121E8 0EE40EEF */ .word 0x0EE40EEF
    /* 2016EEC 806121EC 0EFA0F05 */ .word 0x0EFA0F05
    /* 2016EF0 806121F0 0F100F1A */ .word 0x0F100F1A
    /* 2016EF4 806121F4 0F250F30 */ .word 0x0F250F30
    /* 2016EF8 806121F8 0F3B0F46 */ .word 0x0F3B0F46
    /* 2016EFC 806121FC 0F510F5C */ .word 0x0F510F5C
    /* 2016F00 80612200 0F670F72 */ .word 0x0F670F72
    /* 2016F04 80612204 0F7D0F88 */ .word 0x0F7D0F88
    /* 2016F08 80612208 0F930F9E */ .word 0x0F930F9E
    /* 2016F0C 8061220C 0FA90FB4 */ .word 0x0FA90FB4
    /* 2016F10 80612210 0FBF0FCA */ .word 0x0FBF0FCA
    /* 2016F14 80612214 0FD50FE0 */ .word 0x0FD50FE0
    /* 2016F18 80612218 0FEB0FF6 */ .word 0x0FEB0FF6
    /* 2016F1C 8061221C 1001100C */ .word 0x1001100C
    /* 2016F20 80612220 10171022 */ .word 0x10171022
    /* 2016F24 80612224 102D1038 */ .word 0x102D1038
    /* 2016F28 80612228 1043104E */ .word 0x1043104E
    /* 2016F2C 8061222C 10591064 */ .word 0x10591064
    /* 2016F30 80612230 106F107B */ .word 0x106F107B
    /* 2016F34 80612234 10861091 */ .word 0x10861091
    /* 2016F38 80612238 109C10A7 */ .word 0x109C10A7
    /* 2016F3C 8061223C 10B210BD */ .word 0x10B210BD
    /* 2016F40 80612240 10C810D3 */ .word 0x10C810D3
    /* 2016F44 80612244 10DE10EA */ .word 0x10DE10EA
    /* 2016F48 80612248 10F51100 */ .word 0x10F51100
    /* 2016F4C 8061224C 110B1116 */ .word 0x110B1116
    /* 2016F50 80612250 1121112C */ .word 0x1121112C
    /* 2016F54 80612254 11381143 */ .word 0x11381143
    /* 2016F58 80612258 114E1159 */ .word 0x114E1159
    /* 2016F5C 8061225C 11641170 */ .word 0x11641170
    /* 2016F60 80612260 117B1186 */ .word 0x117B1186
    /* 2016F64 80612264 1191119C */ .word 0x1191119C
    /* 2016F68 80612268 11A811B3 */ .word 0x11A811B3
    /* 2016F6C 8061226C 11BE11C9 */ .word 0x11BE11C9
    /* 2016F70 80612270 11D511E0 */ .word 0x11D511E0
    /* 2016F74 80612274 11EB11F6 */ .word 0x11EB11F6
    /* 2016F78 80612278 1202120D */ .word 0x1202120D
    /* 2016F7C 8061227C 12181223 */ .word 0x12181223
    /* 2016F80 80612280 122F123A */ .word 0x122F123A
    /* 2016F84 80612284 12451251 */ .word 0x12451251
    /* 2016F88 80612288 125C1267 */ .word 0x125C1267
    /* 2016F8C 8061228C 1273127E */ .word 0x1273127E
    /* 2016F90 80612290 12891295 */ .word 0x12891295
    /* 2016F94 80612294 12A012AB */ .word 0x12A012AB
    /* 2016F98 80612298 12B712C2 */ .word 0x12B712C2
    /* 2016F9C 8061229C 12CD12D9 */ .word 0x12CD12D9
    /* 2016FA0 806122A0 12E412F0 */ .word 0x12E412F0
    /* 2016FA4 806122A4 12FB1306 */ .word 0x12FB1306
    /* 2016FA8 806122A8 1312131D */ .word 0x1312131D
    /* 2016FAC 806122AC 13291334 */ .word 0x13291334
    /* 2016FB0 806122B0 133F134B */ .word 0x133F134B
    /* 2016FB4 806122B4 13561362 */ .word 0x13561362
    /* 2016FB8 806122B8 136D1379 */ .word 0x136D1379
    /* 2016FBC 806122BC 13841390 */ .word 0x13841390
    /* 2016FC0 806122C0 139B13A7 */ .word 0x139B13A7
    /* 2016FC4 806122C4 13B213BE */ .word 0x13B213BE
    /* 2016FC8 806122C8 13C913D5 */ .word 0x13C913D5
    /* 2016FCC 806122CC 13E013EC */ .word 0x13E013EC
    /* 2016FD0 806122D0 13F71403 */ .word 0x13F71403
    /* 2016FD4 806122D4 140E141A */ .word 0x140E141A
    /* 2016FD8 806122D8 14261431 */ .word 0x14261431
    /* 2016FDC 806122DC 143D1448 */ .word 0x143D1448
    /* 2016FE0 806122E0 1454145F */ .word 0x1454145F
    /* 2016FE4 806122E4 146B1477 */ .word 0x146B1477
    /* 2016FE8 806122E8 1482148E */ .word 0x1482148E
    /* 2016FEC 806122EC 149A14A5 */ .word 0x149A14A5
    /* 2016FF0 806122F0 14B114BD */ .word 0x14B114BD
    /* 2016FF4 806122F4 14C814D4 */ .word 0x14C814D4
    /* 2016FF8 806122F8 14E014EB */ .word 0x14E014EB
    /* 2016FFC 806122FC 14F71503 */ .word 0x14F71503
    /* 2017000 80612300 150E151A */ .word 0x150E151A
    /* 2017004 80612304 15261532 */ .word 0x15261532
    /* 2017008 80612308 153D1549 */ .word 0x153D1549
    /* 201700C 8061230C 15551561 */ .word 0x15551561
    /* 2017010 80612310 156C1578 */ .word 0x156C1578
    /* 2017014 80612314 15841590 */ .word 0x15841590
    /* 2017018 80612318 159C15A7 */ .word 0x159C15A7
    /* 201701C 8061231C 15B315BF */ .word 0x15B315BF
    /* 2017020 80612320 15CB15D7 */ .word 0x15CB15D7
    /* 2017024 80612324 15E315EE */ .word 0x15E315EE
    /* 2017028 80612328 15FA1606 */ .word 0x15FA1606
    /* 201702C 8061232C 1612161E */ .word 0x1612161E
    /* 2017030 80612330 162A1636 */ .word 0x162A1636
    /* 2017034 80612334 1642164E */ .word 0x1642164E
    /* 2017038 80612338 16591665 */ .word 0x16591665
    /* 201703C 8061233C 1671167D */ .word 0x1671167D
    /* 2017040 80612340 16891695 */ .word 0x16891695
    /* 2017044 80612344 16A116AD */ .word 0x16A116AD
    /* 2017048 80612348 16B916C5 */ .word 0x16B916C5
    /* 201704C 8061234C 16D116DD */ .word 0x16D116DD
    /* 2017050 80612350 16E916F5 */ .word 0x16E916F5
    /* 2017054 80612354 1701170E */ .word 0x1701170E
    /* 2017058 80612358 171A1726 */ .word 0x171A1726
    /* 201705C 8061235C 1732173E */ .word 0x1732173E
    /* 2017060 80612360 174A1756 */ .word 0x174A1756
    /* 2017064 80612364 1762176E */ .word 0x1762176E
    /* 2017068 80612368 177A1787 */ .word 0x177A1787
    /* 201706C 8061236C 1793179F */ .word 0x1793179F
    /* 2017070 80612370 17AB17B7 */ .word 0x17AB17B7
    /* 2017074 80612374 17C417D0 */ .word 0x17C417D0
    /* 2017078 80612378 17DC17E8 */ .word 0x17DC17E8
    /* 201707C 8061237C 17F41801 */ .word 0x17F41801
    /* 2017080 80612380 180D1819 */ .word 0x180D1819 /* invalid instruction */
    /* 2017084 80612384 18251832 */ .word 0x18251832 /* invalid instruction */
    /* 2017088 80612388 183E184A */ .word 0x183E184A /* invalid instruction */
    /* 201708C 8061238C 18571863 */ .word 0x18571863 /* invalid instruction */
    /* 2017090 80612390 186F187C */ .word 0x186F187C /* invalid instruction */
    /* 2017094 80612394 18881894 */ .word 0x18881894 /* invalid instruction */
    /* 2017098 80612398 18A118AD */ .word 0x18A118AD /* invalid instruction */
    /* 201709C 8061239C 18BA18C6 */ .word 0x18BA18C6 /* invalid instruction */
    /* 20170A0 806123A0 18D218DF */ .word 0x18D218DF /* invalid instruction */
    /* 20170A4 806123A4 18EB18F8 */ .word 0x18EB18F8 /* invalid instruction */
    /* 20170A8 806123A8 19041911 */ .word 0x19041911 /* invalid instruction */
    /* 20170AC 806123AC 191D192A */ .word 0x191D192A /* invalid instruction */
    /* 20170B0 806123B0 19361943 */ .word 0x19361943 /* invalid instruction */
    /* 20170B4 806123B4 194F195C */ .word 0x194F195C /* invalid instruction */
    /* 20170B8 806123B8 19681975 */ .word 0x19681975 /* invalid instruction */
    /* 20170BC 806123BC 1981198E */ .word 0x1981198E /* invalid instruction */
    /* 20170C0 806123C0 199A19A7 */ .word 0x199A19A7 /* invalid instruction */
    /* 20170C4 806123C4 19B419C0 */ .word 0x19B419C0 /* invalid instruction */
    /* 20170C8 806123C8 19CD19DA */ .word 0x19CD19DA /* invalid instruction */
    /* 20170CC 806123CC 19E619F3 */ .word 0x19E619F3 /* invalid instruction */
    /* 20170D0 806123D0 1A001A0C */ .word 0x1A001A0C
    /* 20170D4 806123D4 1A191A26 */ .word 0x1A191A26 /* invalid instruction */
    /* 20170D8 806123D8 1A321A3F */ .word 0x1A321A3F /* invalid instruction */
    /* 20170DC 806123DC 1A4C1A59 */ .word 0x1A4C1A59 /* invalid instruction */
    /* 20170E0 806123E0 1A651A72 */ .word 0x1A651A72 /* invalid instruction */
    /* 20170E4 806123E4 1A7F1A8C */ .word 0x1A7F1A8C /* invalid instruction */
    /* 20170E8 806123E8 1A991AA5 */ .word 0x1A991AA5 /* invalid instruction */
    /* 20170EC 806123EC 1AB21ABF */ .word 0x1AB21ABF /* invalid instruction */
    /* 20170F0 806123F0 1ACC1AD9 */ .word 0x1ACC1AD9 /* invalid instruction */
    /* 20170F4 806123F4 1AE61AF3 */ .word 0x1AE61AF3 /* invalid instruction */
    /* 20170F8 806123F8 1B001B0C */ .word 0x1B001B0C
    /* 20170FC 806123FC 1B191B26 */ .word 0x1B191B26 /* invalid instruction */
    /* 2017100 80612400 1B331B40 */ .word 0x1B331B40 /* invalid instruction */
    /* 2017104 80612404 1B4D1B5A */ .word 0x1B4D1B5A /* invalid instruction */
    /* 2017108 80612408 1B671B74 */ .word 0x1B671B74 /* invalid instruction */
    /* 201710C 8061240C 1B811B8E */ .word 0x1B811B8E /* invalid instruction */
    /* 2017110 80612410 1B9C1BA9 */ .word 0x1B9C1BA9 /* invalid instruction */
    /* 2017114 80612414 1BB61BC3 */ .word 0x1BB61BC3 /* invalid instruction */
    /* 2017118 80612418 1BD01BDD */ .word 0x1BD01BDD /* invalid instruction */
    /* 201711C 8061241C 1BEA1BF7 */ .word 0x1BEA1BF7 /* invalid instruction */
    /* 2017120 80612420 1C051C12 */ .word 0x1C051C12 /* invalid instruction */
    /* 2017124 80612424 1C1F1C2C */ .word 0x1C1F1C2C /* invalid instruction */
    /* 2017128 80612428 1C391C47 */ .word 0x1C391C47 /* invalid instruction */
    /* 201712C 8061242C 1C541C61 */ .word 0x1C541C61 /* invalid instruction */
    /* 2017130 80612430 1C6E1C7C */ .word 0x1C6E1C7C /* invalid instruction */
    /* 2017134 80612434 1C891C96 */ .word 0x1C891C96 /* invalid instruction */
    /* 2017138 80612438 1CA41CB1 */ .word 0x1CA41CB1 /* invalid instruction */
    /* 201713C 8061243C 1CBE1CCC */ .word 0x1CBE1CCC /* invalid instruction */
    /* 2017140 80612440 1CD91CE7 */ .word 0x1CD91CE7 /* invalid instruction */
    /* 2017144 80612444 1CF41D02 */ .word 0x1CF41D02 /* invalid instruction */
    /* 2017148 80612448 1D0F1D1C */ .word 0x1D0F1D1C /* invalid instruction */
    /* 201714C 8061244C 1D2A1D37 */ .word 0x1D2A1D37 /* invalid instruction */
    /* 2017150 80612450 1D451D53 */ .word 0x1D451D53 /* invalid instruction */
    /* 2017154 80612454 1D601D6E */ .word 0x1D601D6E
    /* 2017158 80612458 1D7B1D89 */ .word 0x1D7B1D89 /* invalid instruction */
    /* 201715C 8061245C 1D961DA4 */ .word 0x1D961DA4 /* invalid instruction */
    /* 2017160 80612460 1DB21DBF */ .word 0x1DB21DBF /* invalid instruction */
    /* 2017164 80612464 1DCD1DDB */ .word 0x1DCD1DDB /* invalid instruction */
    /* 2017168 80612468 1DE81DF6 */ .word 0x1DE81DF6 /* invalid instruction */
    /* 201716C 8061246C 1E041E12 */ .word 0x1E041E12 /* invalid instruction */
    /* 2017170 80612470 1E1F1E2D */ .word 0x1E1F1E2D /* invalid instruction */
    /* 2017174 80612474 1E3B1E49 */ .word 0x1E3B1E49 /* invalid instruction */
    /* 2017178 80612478 1E571E65 */ .word 0x1E571E65 /* invalid instruction */
    /* 201717C 8061247C 1E731E80 */ .word 0x1E731E80 /* invalid instruction */
    /* 2017180 80612480 1E8E1E9C */ .word 0x1E8E1E9C /* invalid instruction */
    /* 2017184 80612484 1EAA1EB8 */ .word 0x1EAA1EB8 /* invalid instruction */
    /* 2017188 80612488 1EC61ED4 */ .word 0x1EC61ED4 /* invalid instruction */
    /* 201718C 8061248C 1EE21EF0 */ .word 0x1EE21EF0 /* invalid instruction */
    /* 2017190 80612490 1EFE1F0C */ .word 0x1EFE1F0C /* invalid instruction */
    /* 2017194 80612494 1F1A1F29 */ .word 0x1F1A1F29 /* invalid instruction */
    /* 2017198 80612498 1F371F45 */ .word 0x1F371F45 /* invalid instruction */
    /* 201719C 8061249C 1F531F61 */ .word 0x1F531F61 /* invalid instruction */
    /* 20171A0 806124A0 1F6F1F7E */ .word 0x1F6F1F7E /* invalid instruction */
    /* 20171A4 806124A4 1F8C1F9A */ .word 0x1F8C1F9A /* invalid instruction */
    /* 20171A8 806124A8 1FA81FB7 */ .word 0x1FA81FB7 /* invalid instruction */
    /* 20171AC 806124AC 1FC51FD3 */ .word 0x1FC51FD3 /* invalid instruction */
    /* 20171B0 806124B0 1FE21FF0 */ .word 0x1FE21FF0 /* invalid instruction */
    /* 20171B4 806124B4 1FFE200D */ .word 0x1FFE200D /* invalid instruction */
    /* 20171B8 806124B8 201B202A */ .word 0x201B202A
    /* 20171BC 806124BC 20382047 */ .word 0x20382047
    /* 20171C0 806124C0 20552064 */ .word 0x20552064
    /* 20171C4 806124C4 20722081 */ .word 0x20722081
    /* 20171C8 806124C8 208F209E */ .word 0x208F209E
    /* 20171CC 806124CC 20AD20BB */ .word 0x20AD20BB
    /* 20171D0 806124D0 20CA20D9 */ .word 0x20CA20D9
    /* 20171D4 806124D4 20E720F6 */ .word 0x20E720F6
    /* 20171D8 806124D8 21052114 */ .word 0x21052114
    /* 20171DC 806124DC 21232131 */ .word 0x21232131
    /* 20171E0 806124E0 2140214F */ .word 0x2140214F
    /* 20171E4 806124E4 215E216D */ .word 0x215E216D
    /* 20171E8 806124E8 217C218B */ .word 0x217C218B
    /* 20171EC 806124EC 219A21A9 */ .word 0x219A21A9
    /* 20171F0 806124F0 21B821C7 */ .word 0x21B821C7
    /* 20171F4 806124F4 21D621E5 */ .word 0x21D621E5
    /* 20171F8 806124F8 21F42204 */ .word 0x21F42204
    /* 20171FC 806124FC 22132222 */ .word 0x22132222
    /* 2017200 80612500 22312240 */ .word 0x22312240
    /* 2017204 80612504 2250225F */ .word 0x2250225F
    /* 2017208 80612508 226E227E */ .word 0x226E227E
    /* 201720C 8061250C 228D229D */ .word 0x228D229D
    /* 2017210 80612510 22AC22BB */ .word 0x22AC22BB
    /* 2017214 80612514 22CB22DA */ .word 0x22CB22DA
    /* 2017218 80612518 22EA22FA */ .word 0x22EA22FA
    /* 201721C 8061251C 23092319 */ .word 0x23092319
    /* 2017220 80612520 23282338 */ .word 0x23282338
    /* 2017224 80612524 23482358 */ .word 0x23482358
    /* 2017228 80612528 23672377 */ .word 0x23672377
    /* 201722C 8061252C 23872397 */ .word 0x23872397
    /* 2017230 80612530 23A723B7 */ .word 0x23A723B7
    /* 2017234 80612534 23C623D6 */ .word 0x23C623D6
    /* 2017238 80612538 23E623F6 */ .word 0x23E623F6
    /* 201723C 8061253C 24072417 */ .word 0x24072417
    /* 2017240 80612540 24272437 */ .word 0x24272437
    /* 2017244 80612544 24472457 */ .word 0x24472457
    /* 2017248 80612548 24672478 */ .word 0x24672478
    /* 201724C 8061254C 24882498 */ .word 0x24882498
    /* 2017250 80612550 24A924B9 */ .word 0x24A924B9
    /* 2017254 80612554 24CA24DA */ .word 0x24CA24DA
    /* 2017258 80612558 24EB24FB */ .word 0x24EB24FB
    /* 201725C 8061255C 250C251C */ .word 0x250C251C
    /* 2017260 80612560 252D253E */ .word 0x252D253E
    /* 2017264 80612564 254E255F */ .word 0x254E255F
    /* 2017268 80612568 25702581 */ .word 0x25702581
    /* 201726C 8061256C 259125A2 */ .word 0x259125A2
    /* 2017270 80612570 25B325C4 */ .word 0x25B325C4
    /* 2017274 80612574 25D525E6 */ .word 0x25D525E6
    /* 2017278 80612578 25F72608 */ .word 0x25F72608
    /* 201727C 8061257C 2619262B */ .word 0x2619262B
    /* 2017280 80612580 263C264D */ .word 0x263C264D
    /* 2017284 80612584 265E2670 */ .word 0x265E2670
    /* 2017288 80612588 26812693 */ .word 0x26812693
    /* 201728C 8061258C 26A426B5 */ .word 0x26A426B5
    /* 2017290 80612590 26C726D9 */ .word 0x26C726D9
    /* 2017294 80612594 26EA26FC */ .word 0x26EA26FC
    /* 2017298 80612598 270E271F */ .word 0x270E271F
    /* 201729C 8061259C 27312743 */ .word 0x27312743
    /* 20172A0 806125A0 27552767 */ .word 0x27552767
    /* 20172A4 806125A4 2779278B */ .word 0x2779278B
    /* 20172A8 806125A8 279D27AF */ .word 0x279D27AF
    /* 20172AC 806125AC 27C127D3 */ .word 0x27C127D3
    /* 20172B0 806125B0 27E527F8 */ .word 0x27E527F8
    /* 20172B4 806125B4 280A281C */ .word 0x280A281C
    /* 20172B8 806125B8 282F2841 */ .word 0x282F2841
    /* 20172BC 806125BC 28542866 */ .word 0x28542866
    /* 20172C0 806125C0 2879288C */ .word 0x2879288C
    /* 20172C4 806125C4 289F28B1 */ .word 0x289F28B1
    /* 20172C8 806125C8 28C428D7 */ .word 0x28C428D7
    /* 20172CC 806125CC 28EA28FD */ .word 0x28EA28FD
    /* 20172D0 806125D0 29102923 */ .word 0x29102923
    /* 20172D4 806125D4 2936294A */ .word 0x2936294A
    /* 20172D8 806125D8 295D2970 */ .word 0x295D2970
    /* 20172DC 806125DC 29842997 */ .word 0x29842997
    /* 20172E0 806125E0 29AB29BE */ .word 0x29AB29BE
    /* 20172E4 806125E4 29D229E6 */ .word 0x29D229E6
    /* 20172E8 806125E8 29F92A0D */ .word 0x29F92A0D
    /* 20172EC 806125EC 2A212A35 */ .word 0x2A212A35
    /* 20172F0 806125F0 2A492A5D */ .word 0x2A492A5D
    /* 20172F4 806125F4 2A712A85 */ .word 0x2A712A85
    /* 20172F8 806125F8 2A9A2AAE */ .word 0x2A9A2AAE
    /* 20172FC 806125FC 2AC22AD7 */ .word 0x2AC22AD7
    /* 2017300 80612600 2AEC2B00 */ .word 0x2AEC2B00
    /* 2017304 80612604 2B152B2A */ .word 0x2B152B2A
    /* 2017308 80612608 2B3E2B53 */ .word 0x2B3E2B53
    /* 201730C 8061260C 2B682B7D */ .word 0x2B682B7D
    /* 2017310 80612610 2B932BA8 */ .word 0x2B932BA8
    /* 2017314 80612614 2BBD2BD3 */ .word 0x2BBD2BD3
    /* 2017318 80612618 2BE82BFE */ .word 0x2BE82BFE
    /* 201731C 8061261C 2C132C29 */ .word 0x2C132C29
    /* 2017320 80612620 2C3F2C55 */ .word 0x2C3F2C55
    /* 2017324 80612624 2C6B2C81 */ .word 0x2C6B2C81
    /* 2017328 80612628 2C972CAD */ .word 0x2C972CAD
    /* 201732C 8061262C 2CC42CDA */ .word 0x2CC42CDA
    /* 2017330 80612630 2CF12D07 */ .word 0x2CF12D07
    /* 2017334 80612634 2D1E2D35 */ .word 0x2D1E2D35
    /* 2017338 80612638 2D4C2D63 */ .word 0x2D4C2D63
    /* 201733C 8061263C 2D7A2D91 */ .word 0x2D7A2D91
    /* 2017340 80612640 2DA82DC0 */ .word 0x2DA82DC0
    /* 2017344 80612644 2DD82DEF */ .word 0x2DD82DEF
    /* 2017348 80612648 2E072E1F */ .word 0x2E072E1F
    /* 201734C 8061264C 2E372E4F */ .word 0x2E372E4F
    /* 2017350 80612650 2E672E80 */ .word 0x2E672E80
    /* 2017354 80612654 2E982EB1 */ .word 0x2E982EB1
    /* 2017358 80612658 2ECA2EE3 */ .word 0x2ECA2EE3
    /* 201735C 8061265C 2EFC2F15 */ .word 0x2EFC2F15
    /* 2017360 80612660 2F2E2F48 */ .word 0x2F2E2F48
    /* 2017364 80612664 2F612F7B */ .word 0x2F612F7B
    /* 2017368 80612668 2F952FAF */ .word 0x2F952FAF
    /* 201736C 8061266C 2FC92FE4 */ .word 0x2FC92FE4
    /* 2017370 80612670 2FFE3019 */ .word 0x2FFE3019
    /* 2017374 80612674 3034304F */ .word 0x3034304F
    /* 2017378 80612678 306A3085 */ .word 0x306A3085
    /* 201737C 8061267C 30A130BD */ .word 0x30A130BD
    /* 2017380 80612680 30D930F5 */ .word 0x30D930F5
    /* 2017384 80612684 3111312D */ .word 0x3111312D
    /* 2017388 80612688 314A3167 */ .word 0x314A3167
    /* 201738C 8061268C 318431A2 */ .word 0x318431A2
    /* 2017390 80612690 31BF31DD */ .word 0x31BF31DD
    /* 2017394 80612694 31FB321A */ .word 0x31FB321A
    /* 2017398 80612698 32383257 */ .word 0x32383257
    /* 201739C 8061269C 32763295 */ .word 0x32763295
    /* 20173A0 806126A0 32B532D5 */ .word 0x32B532D5
    /* 20173A4 806126A4 32F53316 */ .word 0x32F53316
    /* 20173A8 806126A8 33373358 */ .word 0x33373358
    /* 20173AC 806126AC 3379339B */ .word 0x3379339B
    /* 20173B0 806126B0 33BD33E0 */ .word 0x33BD33E0
    /* 20173B4 806126B4 34033426 */ .word 0x34033426
    /* 20173B8 806126B8 344A346E */ .word 0x344A346E
    /* 20173BC 806126BC 349334B8 */ .word 0x349334B8
    /* 20173C0 806126C0 34DD3503 */ .word 0x34DD3503
    /* 20173C4 806126C4 352A3551 */ .word 0x352A3551
    /* 20173C8 806126C8 357835A0 */ .word 0x357835A0
    /* 20173CC 806126CC 35C935F2 */ .word 0x35C935F2
    /* 20173D0 806126D0 361D3647 */ .word 0x361D3647
    /* 20173D4 806126D4 3673369F */ .word 0x3673369F
    /* 20173D8 806126D8 36CC36FA */ .word 0x36CC36FA
    /* 20173DC 806126DC 37293759 */ .word 0x37293759
    /* 20173E0 806126E0 378A37BB */ .word 0x378A37BB
    /* 20173E4 806126E4 37EF3823 */ .word 0x37EF3823
    /* 20173E8 806126E8 38593890 */ .word 0x38593890
    /* 20173EC 806126EC 38C93904 */ .word 0x38C93904
    /* 20173F0 806126F0 39413980 */ .word 0x39413980
    /* 20173F4 806126F4 39C13A05 */ .word 0x39C13A05
    /* 20173F8 806126F8 3A4D3A98 */ .word 0x3A4D3A98
    /* 20173FC 806126FC 3AE73B3B */ .word 0x3AE73B3B
    /* 2017400 80612700 3B963BF8 */ .word 0x3B963BF8
    /* 2017404 80612704 3C653CE1 */ .word 0x3C653CE1 /* invalid instruction */
    /* 2017408 80612708 3D733E33 */ .word 0x3D733E33 /* invalid instruction */
    /* 201740C 8061270C 40000000 */ .word 0x40000000
.size D_global_asm_80611F0C, . - D_global_asm_80611F0C

glabel func_global_asm_80612710
    /* 2017410 80612710 3E333E36 */ .word 0x3E333E36 /* invalid instruction */
    /* 2017414 80612714 3E3A3E3E */ .word 0x3E3A3E3E /* invalid instruction */
    /* 2017418 80612718 3E413E45 */ .word 0x3E413E45 /* invalid instruction */
    /* 201741C 8061271C 3E493E4D */ .word 0x3E493E4D /* invalid instruction */
    /* 2017420 80612720 3E513E55 */ .word 0x3E513E55 /* invalid instruction */
    /* 2017424 80612724 3E593E5D */ .word 0x3E593E5D /* invalid instruction */
    /* 2017428 80612728 3E613E65 */ .word 0x3E613E65 /* invalid instruction */
    /* 201742C 8061272C 3E693E6D */ .word 0x3E693E6D /* invalid instruction */
    /* 2017430 80612730 3E713E76 */ .word 0x3E713E76 /* invalid instruction */
    /* 2017434 80612734 3E7A3E7E */ .word 0x3E7A3E7E /* invalid instruction */
    /* 2017438 80612738 3E833E87 */ .word 0x3E833E87 /* invalid instruction */
    /* 201743C 8061273C 3E8C3E90 */ .word 0x3E8C3E90 /* invalid instruction */
    /* 2017440 80612740 3E953E99 */ .word 0x3E953E99 /* invalid instruction */
    /* 2017444 80612744 3E9E3EA3 */ .word 0x3E9E3EA3 /* invalid instruction */
    /* 2017448 80612748 3EA83EAD */ .word 0x3EA83EAD /* invalid instruction */
    /* 201744C 8061274C 3EB23EB7 */ .word 0x3EB23EB7 /* invalid instruction */
    /* 2017450 80612750 3EBC3EC1 */ .word 0x3EBC3EC1 /* invalid instruction */
    /* 2017454 80612754 3EC73ECC */ .word 0x3EC73ECC /* invalid instruction */
    /* 2017458 80612758 3ED23ED7 */ .word 0x3ED23ED7 /* invalid instruction */
    /* 201745C 8061275C 3EDD3EE3 */ .word 0x3EDD3EE3 /* invalid instruction */
    /* 2017460 80612760 3EE93EEF */ .word 0x3EE93EEF /* invalid instruction */
    /* 2017464 80612764 3EF53EFC */ .word 0x3EF53EFC /* invalid instruction */
    /* 2017468 80612768 3F023F09 */ .word 0x3F023F09 /* invalid instruction */
    /* 201746C 8061276C 3F103F17 */ .word 0x3F103F17 /* invalid instruction */
    /* 2017470 80612770 3F1F3F26 */ .word 0x3F1F3F26 /* invalid instruction */
    /* 2017474 80612774 3F2E3F36 */ .word 0x3F2E3F36 /* invalid instruction */
    /* 2017478 80612778 3F3F3F48 */ .word 0x3F3F3F48 /* invalid instruction */
    /* 201747C 8061277C 3F513F5B */ .word 0x3F513F5B /* invalid instruction */
    /* 2017480 80612780 3F663F71 */ .word 0x3F663F71 /* invalid instruction */
    /* 2017484 80612784 3F7E3F8B */ .word 0x3F7E3F8B /* invalid instruction */
    /* 2017488 80612788 3F9B3FAD */ .word 0x3F9B3FAD /* invalid instruction */
    /* 201748C 8061278C 3FC53FFF */ .word 0x3FC53FFF /* invalid instruction */
.size func_global_asm_80612710, . - func_global_asm_80612710
